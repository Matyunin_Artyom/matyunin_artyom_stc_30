import java.util.Scanner;
import java.util.Arrays;

class SecondTask {
    
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("please, enter array Size:");

        int aSize = scan.nextInt();//вводим размер массива aSize

        int a[] = new int[aSize];//инициализируем массив размера aSize

        System.out.println("please, enter elements of array:");
  
        for (int i=0; i<a.length; i++) {

            a[i]=scan.nextInt();

        }


        sortArray(a, aSize);

        System.out.println("please, enter number to find:");

        int fNumber = scan.nextInt(); 

        search (a, 0, a.length-1, fNumber);
        
    }  


    public static void sortArray (int a[], int i) {

        System.out.println("Entered array:");

        System.out.println(Arrays.toString(a));

        for (i = 0; i < a.length; i++) {

            int min = a[i];

            int indexOfMin = i;
            
            for (int j = i; j < a.length; j++) {

                if (a[j] < min) {

                    min = a[j];

                    indexOfMin = j;
                }
            }

            a[indexOfMin] = a[i];

            a[i] = min;

        }

        System.out.println("Sorted array:");
        System.out.println(Arrays.toString(a));

    }

    public static int search (int[] a, int left, int right, int findNumber) {
       
        

        if (right >= left) {

            int middle = (right + left)/2;
   
            if (findNumber == a[middle]) {

            System.out.println("The element <<"+ findNumber + ">> found at index <<" + middle+">>");
            return middle;            

            }   

            if (a[middle] < findNumber) {

               return search (a,  middle + 1, right,  findNumber);

            }       

            if (a[middle] > findNumber) {

               return search (a, left, middle - 1,  findNumber);

            }
               

        } 

        System.out.println("The element <<"+ findNumber+">> not found");
        return -1;
        
    }  
 
}
  

