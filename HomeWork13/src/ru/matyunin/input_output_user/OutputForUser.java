package ru.matyunin.input_output_user;

public interface OutputForUser {
   String outFindById(Long byId);
    void outGetAll();
    String outFindByCourseName(String findCourseName);
}
