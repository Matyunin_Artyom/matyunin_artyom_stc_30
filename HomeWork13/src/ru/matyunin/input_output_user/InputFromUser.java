package ru.matyunin.input_output_user;

public interface InputFromUser {
    void courseInput();
    void lessonInput();
    void teacherInput();
}