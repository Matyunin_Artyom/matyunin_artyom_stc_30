package ru.matyunin.input_output_user;

public class OutputForUserImpl extends ScannerAndClassesOpen implements OutputForUser {
    public void outGetAll() {
        System.out.println(courseDao.getAll());

    }

    @Override
    public String outFindByCourseName(String findCourseName) {

        return courseDao.findByCourseName(findCourseName).get().getCourseName();
    }

    public String outFindById(Long byId) {
        return courseDao.findById(byId).toString();

    }

}



