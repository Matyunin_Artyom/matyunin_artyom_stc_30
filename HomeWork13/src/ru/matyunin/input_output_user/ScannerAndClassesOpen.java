package ru.matyunin.input_output_user;

import ru.matyunin.dao.*;
import ru.matyunin.utils.IdGeneratorImpl;

import java.util.Scanner;

public class ScannerAndClassesOpen {
    Scanner scanner = new Scanner(System.in);
    CourseDao courseDao =
            new CourseDaoImpl("courses.txt",
                    new IdGeneratorImpl("courses_sequence.txt"));
    LessonDao lessonDao =
            new LessonDaoImpl("lessons.txt",
                    new IdGeneratorImpl("lessons_sequence.txt"));
    TeacherDao teacherDao =
            new TeacherDaoImpl("teachers.txt",
                    new IdGeneratorImpl("teachers_sequence.txt"));

    public boolean stopInput() {
        System.out.println("Нажмите 'Enter' или введите 'N для выхода");
        String command = scanner.nextLine();
        if (command.equals("N")) {
            return false;
        }
        return true;
    }
}
