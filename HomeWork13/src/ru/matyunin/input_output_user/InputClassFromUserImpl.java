package ru.matyunin.input_output_user;

import ru.matyunin.models.Course;
import ru.matyunin.models.Lesson;
import ru.matyunin.models.Teacher;

public class InputClassFromUserImpl extends ScannerAndClassesOpen implements InputFromUser {

    public void courseInput() {
        while (stopInput()) {
            System.out.println("Введите название курса");
            String nameCourse = scanner.nextLine();
            System.out.println("Введите дату начала курса в формате dd/mm/yyyy");
            String beginDate = scanner.nextLine();
            System.out.println("Введите дату окончания курса в формате dd/mm/yyyy");
            String endDate = scanner.nextLine();
            Course course = new Course(nameCourse, beginDate, endDate);
            courseDao.insert(course);
            stopInput();

        }
    }

    @Override
    public void lessonInput() {

        while (stopInput()) {
            System.out.println("Введите название урока");
            String lessonTitle = scanner.nextLine();
            System.out.println("Введите дату и время проведения в формате dd//mm/yyyy||hh:mm");
            String lessonDataTime = scanner.nextLine();
            System.out.println("Введие Id курса");
            long lessonCourseId = scanner.nextLong();
            scanner.nextLine();
            System.out.println("Введите Id пеподавателя");
            long lessonTeacherId = scanner.nextLong();
            scanner.nextLine();

            Lesson lesson = new Lesson(lessonTitle, lessonDataTime, lessonCourseId, lessonTeacherId);
            lessonDao.insert(lesson);
        }
    }

    @Override
    public void teacherInput() {
        while (stopInput()) {
            System.out.println("Введите имя");
            String firstName = scanner.nextLine();
            System.out.println("Введите фамилию");
            String lastName = scanner.nextLine();
            System.out.println("Введите Id курса");
            String courseTeacherName = scanner.nextLine();
            System.out.println("Введите стаж преподавателя");
            int teacherExperience = scanner.nextInt();
            scanner.nextLine();

            Teacher teacher = new Teacher(firstName, lastName, teacherExperience, courseTeacherName);
            teacherDao.insert(teacher);

        }
    }


}
