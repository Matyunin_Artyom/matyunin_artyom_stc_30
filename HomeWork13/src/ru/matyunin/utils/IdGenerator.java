package ru.matyunin.utils;

public interface IdGenerator {
    Long nextId();
}
