package ru.matyunin.dao;

import ru.matyunin.models.Teacher;
import ru.matyunin.utils.IdGenerator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TeacherDaoImpl implements TeacherDao {

    private String fileName;
    private IdGenerator idGenerator;

    public TeacherDaoImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    private Mapper<Teacher, String> teacherToStringMapper = teacher ->
            teacher.getTeacherId() + " " +
                    teacher.getTeacherFirstName() + " " +
                    teacher.getTeacherLastName() + " " +
                    teacher.getTeacherExperience() + " " +
                    teacher.getTeacherCourseName() + "\r\n";

    private Mapper<String, Teacher> stringToTeacherMapper = string -> {
        String data[] = string.split(" ");
        return new Teacher(Long.parseLong(data[0]), data[1], data[2], Integer.parseInt(String.valueOf(data[3])), data[4]);
    };

    @Override
    public void insert(Teacher teacher) {
        try {
            teacher.setTeacherId(idGenerator.nextId());
            OutputStream outputStream = new FileOutputStream(fileName, true);
            outputStream.write(teacherToStringMapper.map(teacher).getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void update(Teacher element) {

    }

    @Override
    public void delete(Teacher element) {

    }

    @Override
    public List<Teacher> getAll() {
        try {
            List<Teacher> teachers = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                Teacher teacher = stringToTeacherMapper.map(current);
                teachers.add(teacher);
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return teachers;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Teacher> findById(Long id) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String data[] = current.split(" ");
                Long existedId = Long.parseLong(data[0]);
                if (existedId.equals(id)) {
                    Teacher teacher = stringToTeacherMapper.map(current);
                    return Optional.of(teacher);
                }

                current = bufferedReader.readLine();
            }

            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Teacher> findByTeacherLastName(String teacherLastName) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                String data[] = current.split(" ");
                String existedId = data[2];
                if (existedId.equals(teacherLastName)) {
                    Teacher teacher = stringToTeacherMapper.map(current);
                    return Optional.of(teacher);
                }

                current = bufferedReader.readLine();
            }

            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}

