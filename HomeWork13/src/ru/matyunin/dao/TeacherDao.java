package ru.matyunin.dao;

import ru.matyunin.models.Teacher;

import java.util.Optional;

public interface TeacherDao extends CrudDao<Teacher> {
    Optional<Teacher> findByTeacherLastName(String teacherLastName);

}
