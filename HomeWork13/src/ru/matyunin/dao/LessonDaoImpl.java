package ru.matyunin.dao;

import ru.matyunin.models.Lesson;
import ru.matyunin.utils.IdGenerator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LessonDaoImpl implements LessonDao {
    private String fileName;
    private IdGenerator idGenerator;

    public LessonDaoImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    private Mapper<Lesson, String> lessonToStringMapper = lesson ->
            lesson.getLessonsId()+ " " +
                    lesson.getLessonTitle() + " " +
                    lesson.getLessonDateTime() + " " +
                    lesson.getLessonCourseId() + " " +
                    lesson.getLessonTeacherId() + "\r\n";

    private Mapper<String, Lesson> stringToLessonMapper = string -> {
        String data[] = string.split(" ");
        return new Lesson(Long.parseLong(data[0]), data[1], data[2], Long.parseLong(data[3]), Long.parseLong(data[4]));
    };

    @Override
    public void insert(Lesson lesson) {
        try {
            lesson.setLessonsId(idGenerator.nextId());
            OutputStream outputStream = new FileOutputStream(fileName, true);
            outputStream.write(lessonToStringMapper.map(lesson).getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void update(Lesson element) {

    }

    @Override
    public void delete(Lesson element) {

    }

    @Override
    public List<Lesson> getAll() {
        try {
            List<Lesson> lessons = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                Lesson lesson = stringToLessonMapper.map(current);

                lessons.add(lesson);
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return lessons;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Lesson> findById(Long id) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String data[] = current.split(" ");
                Long existedId = Long.parseLong(data[0]);
                if (existedId.equals(id)) {
                    Lesson lesson = stringToLessonMapper.map(current);
                    return Optional.of(lesson);
                }
                current = bufferedReader.readLine();
            }

            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public Optional<Lesson> findByIdId() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                String data[] = current.split(" ");
                Lesson lesson = stringToLessonMapper.map(current);

                //   }
                current = bufferedReader.readLine();
                return Optional.of(lesson);
            }

            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
