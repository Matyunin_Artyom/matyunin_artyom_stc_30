package ru.matyunin.dao;
import java.util.List;
import java.util.Optional;

public interface CrudDao<T> {
    void insert(T element);
    void update(T element);
    void delete(T element);
    List<T> getAll();
    Optional<T> findById(Long id);
}
