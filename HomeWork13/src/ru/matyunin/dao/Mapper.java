package ru.matyunin.dao;

public interface Mapper<X,Y> {

    Y map(X x);
}
