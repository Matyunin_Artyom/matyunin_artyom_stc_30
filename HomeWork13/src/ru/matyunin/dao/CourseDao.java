package ru.matyunin.dao;

import ru.matyunin.models.Course;

import java.util.Optional;

public interface CourseDao extends CrudDao<Course>{
     Optional<Course> findByCourseName(String courseName);
}
