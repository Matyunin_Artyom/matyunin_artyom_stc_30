package ru.matyunin.dao;

import ru.matyunin.models.Course;
import ru.matyunin.utils.IdGenerator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CourseDaoImpl implements CourseDao {

    private String fileName;
    private IdGenerator idGenerator;

    public CourseDaoImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    private Mapper<Course, String> courseToStringMapper = course ->
            course.getCourseId() + " " +
                    course.getCourseName() + " " +
                    course.getCourseBeginDate() + " " +
                    course.getCourseEndDate()+ "\r\n";

    private Mapper<String, Course> stringToCourseMapper = string -> {
        String data[] = string.split(" ");
        return new Course(Long.parseLong(data[0]), data[1], data[2], data[3]);
    };

    @Override
    public void insert(Course course) {

            try {
                course.setCourseId(idGenerator.nextId());
                OutputStream outputStream = new FileOutputStream(fileName, true);
                outputStream.write(courseToStringMapper.map(course).getBytes());
                outputStream.close();
            } catch (FileNotFoundException e) {
                throw new IllegalArgumentException(e);
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
    }

    @Override
    public void update(Course element) {

    }

    @Override
    public void delete(Course element) {

    }

    @Override
    public List<Course> getAll() {
        try {
            List<Course> courses = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                Course course = stringToCourseMapper.map(current);
                courses.add(course);
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return courses;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Course> findById(Long id) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String data[] = current.split(" ");
                Long existedId = Long.parseLong(data[0]);
                if (existedId.equals(id)) {
                    Course course = stringToCourseMapper.map(current);
                    return Optional.of(course);
                }

                current = bufferedReader.readLine();
            }

            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }


    @Override
    public Optional<Course> findByCourseName(String findCourseName) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();

            while (current != null) {
                String data[] = current.split(" ");
                String courseName = String.valueOf(data[1]);
                if (courseName.equals(findCourseName)) {
                    Course course = stringToCourseMapper.map(current);
                    return Optional.of(course);
                }

                current = bufferedReader.readLine();
            }

            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
