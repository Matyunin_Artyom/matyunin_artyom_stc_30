package ru.matyunin.dao;

import ru.matyunin.models.Lesson;

public interface LessonDao extends CrudDao<Lesson> {

}
