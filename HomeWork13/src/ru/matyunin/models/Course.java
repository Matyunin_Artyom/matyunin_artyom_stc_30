package ru.matyunin.models;

import java.util.List;
import java.util.Objects;

public class Course {
    private Long courseId;
    private String courseName;
    private String courseBeginDate;
    private String courseEndDate;
    private List<Teacher> courseTeachersList;
    private List<Lesson> courseLessonsList;

    public Course(Long courseId, String courseName, String courseBeginDate, String courseEndDate) {
        this.courseId = courseId;
        this.courseName = courseName;
        this.courseBeginDate = courseBeginDate;
        this.courseEndDate = courseEndDate;
    }

    public Course(String courseName, String courseBeginDate, String courseEndDate) {
        this.courseName = courseName;
        this.courseBeginDate = courseBeginDate;
        this.courseEndDate = courseEndDate;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseBeginDate() {
        return courseBeginDate;
    }

    public void setCourseBeginDate(String courseBeginDate) {
        this.courseBeginDate = courseBeginDate;
    }

    public String getCourseEndDate() {
        return courseEndDate;
    }

    public void setCourseEndDate(String courseEndDate) {
        this.courseEndDate = courseEndDate;
    }

    public List<Teacher> getCourseTeachersList() {
        return courseTeachersList;
    }

    public void setCourseTeachersList(List<Teacher> courseTeachersList) {
        this.courseTeachersList = courseTeachersList;
    }

    public List<Lesson> getCourseLessonsList() {
        return courseLessonsList;
    }

    public void setCourseLessonsList(List<Lesson> courseLessonsList) {
        this.courseLessonsList = courseLessonsList;
    }

    @Override
    public String toString() {
        return "Course{" +
                "courseId=" + courseId +
                ", courseName='" + courseName + '\'' +
                ", courseBeginDate=" + courseBeginDate +
                ", courseEndDate=" + courseEndDate +
                ", courseTeachersList=" + courseTeachersList +
                ", courseLessonsList=" + courseLessonsList +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return Objects.equals(courseId, course.courseId) &&
                Objects.equals(courseName, course.courseName) &&
                Objects.equals(courseBeginDate, course.courseBeginDate) &&
                Objects.equals(courseEndDate, course.courseEndDate) &&
                Objects.equals(courseTeachersList, course.courseTeachersList) &&
                Objects.equals(courseLessonsList, course.courseLessonsList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(courseId, courseName, courseBeginDate, courseEndDate, courseTeachersList, courseLessonsList);
    }
}

