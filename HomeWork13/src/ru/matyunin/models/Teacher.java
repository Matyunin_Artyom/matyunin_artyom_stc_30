package ru.matyunin.models;

import java.util.List;
import java.util.Objects;

public class Teacher {

    private Long teacherId;
    private String teacherFirstName;
    private String teacherLastName;
    private int teacherExperience;
    private String teacherCourseName;
    private List<Course> teacherCoursesList;

    public Teacher() {

    }

    public Teacher(Long teacherId, String teacherFirstName, String teacherLastName, int teacherExperience, String teacherCourseName) {
        this.teacherId = teacherId;
        this.teacherFirstName = teacherFirstName;
        this.teacherLastName = teacherLastName;
        this.teacherExperience = teacherExperience;
        this.teacherCourseName = teacherCourseName;
    }

    public Teacher(String teacherFirstName, String teacherLastName, int teacherExperience, String teacherCourseName) {
        this.teacherFirstName = teacherFirstName;
        this.teacherLastName = teacherLastName;
        this.teacherExperience = teacherExperience;
        this.teacherCourseName = teacherCourseName;

    }


    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherFirstName() {
        return teacherFirstName;
    }

    public void setTeacherFirstName(String teacherFirstName) {
        this.teacherFirstName = teacherFirstName;
    }

    public String getTeacherLastName() {
        return teacherLastName;
    }

    public void setTeacherLastName(String teacherLastName) {
        this.teacherLastName = teacherLastName;
    }

    public int getTeacherExperience() {
        return teacherExperience;
    }

    public void setTeacherExperience(int teacherExperience) {
        this.teacherExperience = teacherExperience;
    }

    public List<Course> getTeacherCoursesList() {
        return teacherCoursesList;
    }

    public void setTeacherCoursesList(List<Course> teacherCoursesList) {
        this.teacherCoursesList = teacherCoursesList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teacher teacher = (Teacher) o;
        return teacherExperience == teacher.teacherExperience &&
                Objects.equals(teacherId, teacher.teacherId) &&
                Objects.equals(teacherFirstName, teacher.teacherFirstName) &&
                Objects.equals(teacherLastName, teacher.teacherLastName) &&
                Objects.equals(teacherCourseName, teacher.teacherCourseName) &&
                Objects.equals(teacherCoursesList, teacher.teacherCoursesList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(teacherId, teacherFirstName, teacherLastName, teacherExperience, teacherCourseName, teacherCoursesList);
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "teacherId=" + teacherId +
                ", teacherFirstName='" + teacherFirstName + '\'' +
                ", teacherLastName='" + teacherLastName + '\'' +
                ", teacherExperience=" + teacherExperience +
                ", teacherCourseName='" + teacherCourseName + '\'' +
                ", teacherCoursesList=" + teacherCoursesList +
                '}' + "\r\n";
    }

    public String getTeacherCourseName() {
        return teacherCourseName;
    }

    public void setTeacherCourseId(String teacherCourseName) {
        this.teacherCourseName = teacherCourseName;
    }
}
