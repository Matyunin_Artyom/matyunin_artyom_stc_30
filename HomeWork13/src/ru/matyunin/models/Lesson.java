package ru.matyunin.models;

import java.util.Objects;

public class Lesson {
    private long lessonsId;
    private String lessonTitle;
    private String lessonDateTime;
    private long lessonCourseId;
    private long lessonTeacherId;

    public Lesson() {

    }

    public Lesson(String lessonTitle, String lessonDateTime, long lessonCourseId, long lessonTeacherId) {

        this.lessonTitle = lessonTitle;
        this.lessonDateTime = lessonDateTime;
        this.lessonCourseId = lessonCourseId;
        this.lessonTeacherId = lessonTeacherId;
    }

    public Lesson(long lessonsId, String lessonTitle, String lessonDateTime, long lessonCourseId, long lessonTeacherId) {
        this.lessonsId = lessonsId;
        this.lessonTitle = lessonTitle;
        this.lessonDateTime = lessonDateTime;
        this.lessonCourseId = lessonCourseId;
        this.lessonTeacherId = lessonTeacherId;
    }

    public long getLessonsId() {
        return lessonsId;
    }

    public void setLessonsId(long lessonsId) {
        this.lessonsId = lessonsId;
    }

    public void setLessonDateTime(String lessonDateTime) {
        this.lessonDateTime = lessonDateTime;
    }

    public void setLessonTitle(String lessonTitle) {
        this.lessonTitle = lessonTitle;
    }

    public void setLessonCourseId(long lessonCourseId) {
        this.lessonCourseId = lessonCourseId;
    }

    public String getLessonDateTime() {
        return lessonDateTime;
    }

    public String getLessonTitle() {
        return lessonTitle;
    }

    public long getLessonCourseId() {
        return lessonCourseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lesson lesson = (Lesson) o;
        return lessonsId == lesson.lessonsId &&
                lessonCourseId == lesson.lessonCourseId &&
                lessonTeacherId == lesson.lessonTeacherId &&
                Objects.equals(lessonTitle, lesson.lessonTitle) &&
                Objects.equals(lessonDateTime, lesson.lessonDateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lessonsId, lessonTitle, lessonDateTime, lessonCourseId, lessonTeacherId);
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "lessonsId=" + lessonsId +
                ", lessonTitle='" + lessonTitle + '\'' +
                ", lessonDateTime='" + lessonDateTime + '\'' +
                ", lessonCourseId=" + lessonCourseId +
                ", lessonTeacherId=" + lessonTeacherId +
                '}' + "\r\n";
    }

    public long getLessonTeacherId() {
        return lessonTeacherId;
    }

    public void setLessonTeacherId(long lessonTeacherId) {
        this.lessonTeacherId = lessonTeacherId;
    }
}
