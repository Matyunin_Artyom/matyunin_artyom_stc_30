package ru.matyunin;

public interface Iterable<B> {
    Iterator<B> iterator();
}
