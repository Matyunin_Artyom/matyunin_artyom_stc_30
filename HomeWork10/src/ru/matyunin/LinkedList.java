package ru.matyunin;


public class LinkedList<E> implements List<E> {
    private class LinkedListIterator implements Iterator<E> {
        Node<E> current = first;

        @Override
        public E next() {
            E value = current.value;
            current = current.next;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }
    }

    private Node<E> first;
    private Node<E> last;
    private int count;

    private static class Node<F> {
        F value;
        Node<F> next;

        public Node(F value) {
            this.value = value;
        }
    }

    @Override
    public E get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node<E> current = this.first;
            while (current != null && i < index) {
                current = current.next;
                i++;
            }
            return current.value;
        }
        System.err.println("Failed to get. There is no such element in the sequence");
        return null;
    }

    @Override
    public int indexOf(E element) {
        int i = 0;
        Node<E> current = this.first;
        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }
        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void removeByIndex(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node<E> current = this.first;
            Node<E> next = current.next;
            while (current != null && i < index) {
                if (i == index - 1) {
                    current.next = next.next;
                    count--;
                    break;
                } else {
                    current = next;
                    next = next.next;
                    i++;
                }
            }

        } else {
            System.err.println("Not deleted. There is no such element in the sequence.");

        }

    }

    @Override
    public void insert(E element, int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node<E> insertedElement = new Node<>(element);
            Node<E> current = this.first;
            Node<E> next = current.next;
            while (next != null && i < index) {
                if (i == index-1) {
                    current.next = insertedElement;
                    insertedElement.next = next;
                    count++;
                    break;
                } else {
                    current = next;
                    next = next.next;
                    i++;
                }
            }

        } else {
            System.err.println("Not deleted. There is no such element in the sequence.");

        }


    }

    @Override
    public void add(E element) {
        Node<E> newNode = new Node<>(element);
        if (first == null) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        count++;
    }

    @Override
    public void reverse() {
        Node<E> prev = null;
        Node<E> current = first;
        Node<E> next = null;
        while (current != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        this.first = prev;
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) != -1;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(E element) {
        if (this.first.value == element) {
            this.first = this.first.next;
            count--;
        } else {
            int i = 0;
            Node<E> current = this.first;
            Node<E> next = current.next;
            while (current != null && i < count && current.next.value != element) {
                current = current.next;
                next = next.next;
                i++;
            }
            current.next = next.next;
        }
    }

    @Override
    public Iterator<E> iterator() {
        return new LinkedListIterator();
    }
}
