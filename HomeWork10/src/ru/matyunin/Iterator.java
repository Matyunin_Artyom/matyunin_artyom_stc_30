package ru.matyunin;

public interface Iterator<A> {
    A next();
    boolean hasNext();
}
