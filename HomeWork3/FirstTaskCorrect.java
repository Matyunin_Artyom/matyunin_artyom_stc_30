import java.util.Scanner;
import java.util.Arrays;

class FirstTaskCorrect {

      public static void main(String[] args) {
  
             Scanner scan = new Scanner(System.in);

             System.out.println("please, enter array Size:");

             int aSize = scan.nextInt();//вводим размер массива aSize

             int a[] = new int[aSize];//инициализируем массив размера aSize

             System.out.println("please, enter elements of array:");
             //получаем элементы
             for (int i=0; i<a.length; i++) {

                  a[i]=scan.nextInt();
     
              }
              //поочередно будем вызывать каждый метод и вывод результата его работы на консоль
              System.out.println("Entered array :");//вначале отобразим введенный массив

              System.out.println(Arrays.toString(a));

              System.out.println("Sum Elements:" + sumElementsOfArray(a, aSize));

              System.out.println("Average Elements:" + avgElementsOfArray(a, aSize));

              System.out.println("Out of number:" + arrayOutOfNumber(a, aSize));

              System.out.println("Change max and min elements:");

              changeElementsOfArray(a, aSize);

              System.out.println(Arrays.toString(a));

              System.out.println("Revers elements:");

              reverseElementsOfArray(a, aSize);

              System.out.println(Arrays.toString(a));

              System.out.println("Sort elements:");

              sortElementsOfArray(a, aSize);

              System.out.println(Arrays.toString(a));

     
      }

//сумма элементов
      public static int sumElementsOfArray(int a[], int i) { 

            int sumA = 0;

            for (i=0; i<a.length; i++)    {
     
                sumA+=a[i];
     
            }
  
            return sumA;
      }
     //вывод в число
      public static int arrayOutOfNumber (int a[], int i) {

            int number = a[0]; 

            for (i=1; i< a.length; i++) {


    
                int checkNumber = a[i];

                int j = 0;
                
                while (checkNumber!= 0) {    

                       //в этом цикле найдем количество символов в элементе

                       checkNumber=checkNumber/10;
                       j = j + 1;

                }

                int b = 1;
                for (int n=0; n<j; n++) {          //10 возведем в степень j
 
                    b = b*10;

                }
             
                number=(number*b)+a[i];            //умножим предыдущее число на 10в степени j и прибавим текущее число

            }
            return number;
            
      }
      //сортировка пузырьком
      public static void sortElementsOfArray(int a[] , int i) {

            for (int j=1; j<a.length; j++) {

                for (i=0; i<a.length-1; i++) {

                    if (a[i] > a[i+1]) {  // если текущий больше следующего, то меняем их местами

                         int sortElement = a[i];

                         a[i] = a[i+1];

                         a[i+1] = sortElement;
                    }

                }

            }

      }

      // замена максимального и минимального элементов
      public static void changeElementsOfArray(int a[], int i ) {

            int min=Integer.MAX_VALUE;//здесь сохраним минимальный элемент

            int indexOfMin=0;//здесь его индекс
            
            int max=Integer.MIN_VALUE;//здесь сохраним макс. элемент
            
            int indexOfMax=0;//здесь его индекс

            for (int j=0; j<a.length; j++) {

                if (a[j] < min) {
                            
                    min = a[j];
                            
                    indexOfMin=j; //ищем минимальный элемент и его индекс и записываем в переменные

                }   

                if (a[j] > max) { //аналогично для максимального элемента
                        
                    max = a[j];

                    indexOfMax=j;
             
                }

            }
    
                a[indexOfMin]=max;//записываем в минимальный элемент значение максимального
                a[indexOfMax]=min;//записываем в максимальнй элемент значение минимального
  
      }

     //среднее арифметическое
      public static double avgElementsOfArray(int a[], int i) {

            double sumA=0;

            for (i=0; i<a.length; i++) {
 
                sumA+=a[i];

            }

            return sumA/a.length;

            
      }

      //разворот массива
      public static void reverseElementsOfArray(int a[], int i) {


            for (i = 0; i < a.length/2; i++) {     

                 int temp = a[a.length -1- i];

                  a[a.length -1 - i] = a[i];

                  a[i] = temp;
 
            }    
        
      }
      

  }