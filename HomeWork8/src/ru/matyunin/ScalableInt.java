package ru.matyunin;

public interface ScalableInt {
    void changeSize(int changeShortProperties, int changeLongProperties);

}
