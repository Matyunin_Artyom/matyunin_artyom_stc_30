package ru.matyunin;

import java.awt.geom.Area;

public class Application {
    public static void main(String[] args) {
        GeometricFigure ellipse = new Ellipse("ellipse", 10, 15);
        GeometricFigure circle = new Circle("circle", 15);
        GeometricFigure rectangle = new Rectangle("rectangle", 10, 15);
        GeometricFigure square = new Square("square", 15);
        GeometricFigure geometricFigures[] = new GeometricFigure[]{ellipse, circle, rectangle, square};

        System.out.println("Checking area and perimetr");
        for (int i = 0; i < geometricFigures.length; i++) {
            System.out.println("Name: " + "<" + geometricFigures[i].getName() + ">" +
                    " Area: " + geometricFigures[i].getArea() +
                    " Perimetr: " + geometricFigures[i].getPerimetr());
        }

        System.out.println("resize the shapes");
        ScalableInt ellipsScal = ellipse;
        ScalableInt circleScal = circle;
        ScalableInt rectangleScal = rectangle;
        ScalableInt squareScal = square;
        ScalableInt scalableInts[] = new ScalableInt[]{ellipsScal, circleScal, rectangleScal, squareScal};
        for (int i = 0; i < scalableInts.length; i++) {
            scalableInts[i].changeSize(4, -5);
        }
        for (int i = 0; i < geometricFigures.length; i++) {
            System.out.println("Name: " + "<" + geometricFigures[i].getName() + ">"
                    + " Area: " + geometricFigures[i].getArea() +
                    " Perimetr: " + geometricFigures[i].getPerimetr());
        }

        System.out.println("change the coordinates of the shapes");
        RelocatableInt ellipsRel = ellipse;
        RelocatableInt circleRel = circle;
        RelocatableInt rectaglReL = rectangle;
        RelocatableInt squareReL = square;
        RelocatableInt relocatableInts[] = new RelocatableInt[]{ellipsRel, circleRel, rectaglReL, squareReL};
        for (int i = 0; i < relocatableInts.length; i++) {
            relocatableInts[i].changeCoordinateCentr(4, -5);
        }
        for (int i = 0; i < geometricFigures.length; i++) {
            System.out.println("Name: " + "<" + geometricFigures[i].getName() + ">"
                    + " X: " + geometricFigures[i].coordinateX +
                    " Y: " + geometricFigures[i].coordinateY);
        }

    }
}
