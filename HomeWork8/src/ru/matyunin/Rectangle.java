package ru.matyunin;

public class Rectangle extends Square {
    protected int shortEdge;

    public Rectangle(String name, int edge, int shortEdge) {
        super(name, edge);
        this.shortEdge = shortEdge;
    }

    public double getArea() {
        return this.shortEdge * this.edge;
    }

    public double getPerimetr() {
        return (this.shortEdge + this.edge) * 2;
    }

    public void changeSize(int changeShortProperties, int changeLongProperties) {
        this.edge += changeLongProperties;
        this.shortEdge += changeShortProperties;
    }

}
