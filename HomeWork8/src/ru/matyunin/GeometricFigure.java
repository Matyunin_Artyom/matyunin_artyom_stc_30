package ru.matyunin;

public abstract class GeometricFigure implements RelocatableInt, ScalableInt {
    private final String name;
    protected int coordinateX;
    protected int coordinateY;

    public String getName() {
        return name;
    }

    protected GeometricFigure(String name) {
        this.name = name;
        this.coordinateX = 0;
        this.coordinateY = 0;
    }

    public void changeCoordinateCentr(int shiftX, int shiftY) {

        this.coordinateX += shiftX;
        this.coordinateY += shiftY;
    }

    public abstract double getArea();
    public abstract void changeSize(int changeShortProperties, int changeLongProperties);
    public abstract double getPerimetr();

}

