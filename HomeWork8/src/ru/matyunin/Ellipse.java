package ru.matyunin;

import static java.lang.Math.*;

public class Ellipse extends Circle {
    protected int shortRad;

    public Ellipse(String name, int radius, int shortRad) {
        super(name, radius);
        this.shortRad = shortRad;
    }

    @Override
    public double getArea() {
        return PI * this.radius * shortRad;
    }

    public double getPerimetr() {
        return 2 * PI * sqrt((this.radius * this.radius + shortRad * shortRad) / 2);
    }

    public void changeSize(int changeShortProperties, int changeLongProperties) {
        this.radius += changeLongProperties;
        this.shortRad += changeShortProperties;
    }

}
