package ru.matyunin;

public class Square extends GeometricFigure {
    protected int edge;

    public Square(String name, int edge) {
        super(name);
        this.edge = edge;
    }

    public double getArea() {
        return edge * edge;
    }

    public double getPerimetr() {
        return edge * 4;
    }

    public void changeSize(int changeShortProperties, int changeLongProperties) {
        this.edge += changeLongProperties;
    }

}
