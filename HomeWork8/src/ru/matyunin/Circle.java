package ru.matyunin;

import static java.lang.Math.PI;

public class Circle extends GeometricFigure {
    protected int radius;

    public Circle(String name, int radius) {
        super(name);
        this.radius = radius;
    }

    public double getArea() {
        return PI * radius * radius;
    }

    public double getPerimetr() {

        return 2 * PI * radius;
    }

    public void changeSize(int changeShortProperties, int changeLongProperties) {
        this.radius += changeLongProperties;
    }

}
