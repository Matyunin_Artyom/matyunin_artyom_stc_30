package ru.matyunin;

import java.util.Objects;

public class CarsForReport {
    private String number;
    private String model;
    private String color;
    private int mileage;
    private int price;

    public static class Builder {
        private CarsForReport newCarsForReport;

        public Builder() {
            newCarsForReport = new CarsForReport();
        }

        public Builder number(String number) {
            newCarsForReport.number = number;
            return this;
        }

        public Builder model(String model) {
            newCarsForReport.model = model;
            return this;
        }

        public Builder color(String color) {
            newCarsForReport.color = color;
            return this;
        }

        public Builder mileage(int mileage) {
            newCarsForReport.mileage = mileage;
            return this;
        }

        public Builder price(int price) {
            newCarsForReport.price = price;
            return this;
        }

        public CarsForReport build() {
            return newCarsForReport;
        }

        @Override
        public String toString() {
            return "" + newCarsForReport;
        }

    }

    @Override
    public String toString() {
        return "CarsForReport{" +
                "number='" + number + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", mileage=" + mileage +
                ", price=" + price +
                '}' + "\r\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarsForReport that = (CarsForReport) o;
        return mileage == that.mileage &&
                price == that.price &&
                Objects.equals(number, that.number) &&
                Objects.equals(model, that.model) &&
                Objects.equals(color, that.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, model, color, mileage, price);
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
