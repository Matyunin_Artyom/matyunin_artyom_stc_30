package ru.matyunin;

import java.util.Objects;

public class CarsModelsForDistinct {
    private String modelsFromDistinct;

    public CarsModelsForDistinct(String modelsFromDistinct) {
        this.modelsFromDistinct = modelsFromDistinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarsModelsForDistinct that = (CarsModelsForDistinct) o;
        return Objects.equals(modelsFromDistinct, that.modelsFromDistinct);
    }

    @Override
    public int hashCode() {
        return Objects.hash(modelsFromDistinct);
    }

    @Override
    public String toString() {
        return "CarsModelsForDistinct{" +
                "modelsFromDistinct='" + modelsFromDistinct + '\'' +
                '}';

    }
}
