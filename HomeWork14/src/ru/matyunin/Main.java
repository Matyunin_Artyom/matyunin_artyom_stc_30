package ru.matyunin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;

public class Main {

    public static void main(String[] args) {

        try {
            System.out.println("1) Номера всех автомобилей, имеющих черный цвет или нулевой:\n");
            Files.lines(Paths.get("cars.txt"))
                    .map(line -> line.split(" "))
                    .map(array -> new Cars(array[0], array[1], array[2],
                            Integer.parseInt(array[3]), Integer.parseInt(array[4])))
                    .filter(cars -> cars.getMileage() == 0 || cars.getColor().equals("черный"))
                    .map(cars -> new CarsForReport.Builder().number(cars.getNumber()))
                    .forEach(System.out::println);

        } catch (IOException e) {
            throw new IllegalArgumentException();
        }

        try {
            long result = Files.lines(Paths.get("cars.txt"))
                    .map(line -> line.split(" "))
                    .map(array -> new Cars(array[0], array[1], array[2], Integer.parseInt(array[3]), Integer.parseInt(array[4])))
                    .filter(cars -> cars.getPrice() >= 700000 && cars.getPrice() <= 800000)
                    .map(cars -> new CarsModelsForDistinct(cars.getModel()))
                    .distinct().count();
            System.out.println("2) Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс: " + result);

        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
        try {
            String result = Files.lines(Paths.get("cars.txt"))
                    .map(line -> line.split(" "))
                    .map(array -> new Cars(array[0], array[1], array[2], Integer.parseInt(array[3]), Integer.parseInt(array[4])))
                    .min(Comparator.comparing(Cars::getPrice)).get().getColor();
            System.out.println("Вывести цвет автомобиля с минимальной стоимостью: " + result);

        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
        try {
            double result = Files.lines(Paths.get("cars.txt"))
                    .map(line -> line.split(" "))
                    .map(array -> new Cars(array[0], array[1], array[2], Integer.parseInt(array[3]), Integer.parseInt(array[4])))
                    .filter(cars -> cars.getModel().equals("ToyotaCamry"))
                    .mapToInt(Cars::getPrice)
                    .average().getAsDouble();
            System.out.println("4) Средняя стоимость Camry" + result);

        } catch (IOException e) {
            throw new IllegalArgumentException();
        }

    }
}
