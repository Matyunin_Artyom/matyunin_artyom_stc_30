package ru.matyunin;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LinkedListTest {
    String prefix = "string";
    List<String> list = new LinkedList<>();

    @BeforeEach
    void setUp() {
        for (int i = 0; i <= 4; i++) {
            list.add(prefix + i);
        }
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("=====RESULT=====");
    }

    @AfterEach
    void tearDown() {
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

    }

    @Test
    void get() {//получение элемента по индексу
        Assertions.assertEquals("string3", list.get(3));
    }

    @Test
    void indexOf() {//получение индекса элемента
        Assertions.assertEquals(2, list.indexOf("string2"));
    }

    @Test
    void removeByIndex() {//удаление по индексу
        list.removeByIndex(10);
    }

    @Test
    void insert() {//добавление элемента в середину последовательности
        list.insert("Hello", 0);
    }

    @Test
    void reverse() {
        list.reverse();
    }

    @Test
    void contains() {
        Assertions.assertTrue(list.contains("string2"));
        Assertions.assertFalse(list.contains("hello"));
    }

    @Test
    void size() {
        System.out.println("Sequence size is: " + list.size());
    }

    @Test
    void removeFirst() {
        list.removeFirst("hello");
    }

}