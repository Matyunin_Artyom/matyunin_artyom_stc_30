package ru.matyunin;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.matyunin.ArrayList;
import ru.matyunin.Iterator;
import ru.matyunin.List;

class ArrayListTest {
    List<Integer> list = new ArrayList<>();

    @BeforeEach
    void setUp() {
        //заполним массив числами от 0 до 4
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }
        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("=====RESULT=====");
    }

    @AfterEach
    void tearDown() {
        //будем вызывать итератор после выполнения методов для наблюдениями за изменениями последовательности
        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    @Test
    void get() {
        //получение элемента по индексу
        Assertions.assertEquals(3,list.get(3) );
    }

    @Test
    void indexOf() {
        //получение индекса элемента
        Assertions.assertEquals(3, list.indexOf(3));
    }

    @Test
    void removeByIndex() {
        //удаление по индексу
        list.removeByIndex(10);
    }

    @Test
    void insert() {
        //вставка в середину последовательности
        list.insert(25, 2);
    }

    @Test
    void contains() {
        //проверка наличия элемента в последовательности
        Assertions.assertTrue(list.contains(2));
        Assertions.assertFalse(list.contains(55));
    }

    @Test
    void size() {
        Assertions.assertEquals(5, list.size());
    }

    @Test
    void removeFirst() {
        //удаление первого совпавшего элемента
        list.removeFirst(12);
    }
}