package ru.matyunin;

public interface Collection<C> extends Iterable<C> {
    void add(C element);
    boolean contains(C element);
    int size();
    void removeFirst(C element);


}
