package ru.matyunin;


public class LinkedList<E> implements List<E> {
    private class LinkedListIterator implements Iterator<E> {
        Node<E> current = first;

        @Override
        public E next() {
            E value = current.value;
            current = current.next;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }
    }

    private Node<E> first;
    private Node<E> last;
    private int count;

    private static class Node<F> {
        F value;
        Node<F> next;

        public Node(F value) {
            this.value = value;
        }
    }

    @Override
    public E get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node<E> current = this.first;
            while (current != null && i < index) {
                current = current.next;
                i++;
            }
            return current.value;
        }
        System.err.println("Failed to get. There is no such element in the sequence");
        return null;
    }

    @Override
    public int indexOf(E element) {
        int i = 0;
        Node<E> current = this.first;
        while (current != null && !element.equals(current.value)) {
            current = current.next;
            i++;
        }
        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void removeByIndex(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node<E> current = this.first;
            Node<E> next = current.next;
            while (current != null && i < index) {
                if (i == index - 1) {
                    current.next = next.next;
                    count--;
                    break;
                } else {
                    current = next;
                    next = next.next;
                    i++;
                }
            }

        } else {
            System.err.println("Not deleted. There is no such element in the sequence.");

        }
    }

    @Override
    public void insert(E element, int index) {
        Node<E> insertedElement = new Node<>(element);
        Node<E> current = this.first;
        Node<E> next = current.next;
        if (index == 0) {
            insertedElement.next = first;
            this.first = insertedElement;
        } else if (index > 0 && index < count && first != null) {
            int i = 0;
            while (next != null && i < index) {
                if (i == index - 1) {
                    current.next = insertedElement;
                    insertedElement.next = next;
                    break;
                } else {
                    current = next;
                    next = next.next;
                    i++;
                }
            }
        } else {
            add(element);
            System.err.println("There is no such index in the sequence. Item added last.");
        }
        count++;
    }

    @Override
    public void add(E element) {
        Node<E> newNode = new Node<>(element);
        if (first == null) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        count++;
    }

    @Override
    public void reverse() {
        Node<E> prev = null;
        Node<E> current = first;
        Node<E> next = null;
        while (current != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        this.first = prev;
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) != -1;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(E element) {
        if (element.equals(first.value)) {
            first = first.next;
            count--;
        } else {
            int i = 0;
            Node<E> current = first;
            Node<E> next = current.next;
            while (next.next != null && i < count && !element.equals(current.next.value)) {
                current = current.next;
                next = next.next;
                i++;
            }
            if (element.equals(current.next.value)) {
                current.next = next.next;
            } else {
                System.out.println("Not deleted. There is no such element in the sequence");
            }
        }
    }

    @Override
    public Iterator<E> iterator() {
        return new LinkedListIterator();
    }
}
