package ru.matyunin;

public class MainArrayList {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i <= 17; i++) {
            list.add(i);
        }

        System.out.println("Size of array: " + list.size());
        System.out.println("This array: ");
        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        list.reverse();
        list.removeFirst(1);
        System.out.println(list.indexOf(4));
        System.out.println(list.contains(10));
        list.removeByIndex(4);
        list.insert(1000, 10);
        list.insert(2000, 12);
        list.insert(3000, 14);

        System.out.println("Size after method calls: " + list.size());
        System.out.println("This array: ");
        iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
