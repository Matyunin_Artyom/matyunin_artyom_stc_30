package ru.matyunin;

public class ArrayList<G> implements List<G> {
    private static final int DEFAULT_SIZE = 10;
    private G[] data;
    private int count;

    public ArrayList() {
        this.data = (G[]) new Object[DEFAULT_SIZE];
    }

    private class ArrayListIterator implements Iterator<G> {
        private int current = 0;

        @Override
        public G next() {
            G value = data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    @Override
    public G get(int index) {
        if (index < count) {
            return data[index];
        }
        System.err.println("Out of the array");
        return null;
    }

    @Override
    public int indexOf(G element) {

        for (int i = 0; i < count; i++) {
            if (data[i] == element) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void removeByIndex(int index) {
        if (index < 0 || index >= count) {
            System.out.println("The index " + "<" + index + ">" + " is not in the array or it is not filled");
        } else {
            System.arraycopy(data, index + 1, data, index, count - index);
            count--;
            System.out.println("Item at index " + "<" + index + ">" + " removed");
        }

    }

    @Override
    public void insert(G element, int index) {

        if (index > data.length) {
            System.out.println("there is no such index in the array");
        } else if (index >= count - 1) {
            add(element);
            System.out.println("item added to the nearest empty position");
        } else {
            G item = data[index];
            data[index] = element;
            for (int i = index; i < count; i++) {
                element = data[i + 1];
                data[i + 1] = item;
                item = element;
            }
            count++;
            if (count == data.length) {
                resize();
            }
            System.out.println("Item" + " <" + data[index] + "> " + " at " + "index " + " <" + index + "> " + "added");
        }
    }

    @Override
    public void reverse() {
    }

    @Override
    public void add(G element) {
        if (count == data.length - 1) {
            resize();
        }
        data[count] = element;
        count++;
    }

    private void resize() {
        int oldLength = data.length;
        int newLength = oldLength + (oldLength >> 1);   //oldLength/2;
        G[] newData = (G[]) new Object[newLength];
        System.arraycopy(data, 0, newData, 0, oldLength);
        data = newData;
    }

    @Override
    public boolean contains(G element) {
        return indexOf(element) != -1;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void removeFirst(G element) {
        if (!contains(element)) {
            System.err.print("Not exist this element");
        } else {
            int indexOfRemovingElement = indexOf(element);
            if (count - indexOfRemovingElement >= 0)
                System.arraycopy(data, indexOfRemovingElement + 1, data, indexOfRemovingElement, count - indexOfRemovingElement);
            count--;
            System.out.println("Element " + "<" + element + ">" + " is removed");
        }
    }

    @Override
    public Iterator<G> iterator() {
        return new ArrayListIterator();
    }
}
