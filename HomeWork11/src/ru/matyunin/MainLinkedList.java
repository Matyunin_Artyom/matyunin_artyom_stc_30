package ru.matyunin;

public class MainLinkedList {

    public static void main(String[] args) {
        String prefix = "string";
        List<String> list = new LinkedList<>();
        for (int i = 1; i <= 5; i++) {
            list.add(prefix+i);
        }
        System.out.println("Sequence size: " +  list.size());
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        //list.reverse();
        //list.removeFirst(15);
        //list.removeByIndex(15);
        //list.indexOf(15);
        //list.contains(15);
        list.insert("hello",2);
        list.insert("hello",3);

        System.out.println("Sequence size: " +  list.size());
        iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
