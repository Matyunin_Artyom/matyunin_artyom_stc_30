package ru.matyunin.javafx.game.client.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import lombok.Data;
import ru.matyunin.javafx.game.client.sckets.SocketClient;

import java.net.URL;
import java.util.ResourceBundle;

@Data
public class Controller implements Initializable {

    @FXML
    private AnchorPane pane;
    @FXML
    private Button connectButton;
    @FXML
    private TextField playerNameTextField;
    @FXML
    private Button registrButton;
    @FXML
    private TextArea serverLogsTextArea;
    @FXML
    private Label enemyNameLabel;
    @FXML
    private Label enemyHealthLabel;
    @FXML
    private Label playerNameLabel;
    @FXML
    private Label playerHealthLabel;
    @FXML
    private Circle enemyCircle;
    @FXML
    private Circle playerCircle;
    @FXML
    private Pane enemyPane;
    @FXML
    private Pane playerPane;

    private String playerNumber;

    private SocketClient socketClient;
    public EventHandler<KeyEvent> keyEventEventHandler = event -> {
        if (event.getCode() == KeyCode.LEFT) {
            if (playerCircle.getCenterX() > -310) {
                socketClient.sendMessage("move LEFT " + playerNumber);
                playerCircle.setCenterX(playerCircle.getCenterX() - 10);
            }
        } else if (event.getCode() == KeyCode.RIGHT) {
            if (playerCircle.getCenterX() < 285) {
                socketClient.sendMessage("move RIGHT " + playerNumber);
                playerCircle.setCenterX(playerCircle.getCenterX() + 10);
            }

        } else if (event.getCode() == KeyCode.CONTROL) {
            Circle bulletCircle = new Circle();
            bulletCircle.setRadius(5);
            bulletCircle.setCenterX(playerCircle.getCenterX() + playerCircle.getLayoutX());
            bulletCircle.setCenterY((playerCircle.getCenterY() + 5) + playerCircle.getLayoutY());
            bulletCircle.setFill(Color.ORANGE);
            pane.getChildren().add(bulletCircle);

            final boolean[] inTarget = {false};

            Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0.0025), animation -> {
                if (!inTarget[0]) {

                    bulletCircle.setCenterY(bulletCircle.getCenterY() - 1);
                    if (bulletCircle.getBoundsInParent().intersects(enemyCircle.getBoundsInParent())) {
                        bulletCircle.setVisible(false);
                        if (this.playerNumber.equals("PLAYER_1")) {
                            socketClient.sendMessage("hit PLAYER_2");
                        } else {
                            socketClient.sendMessage("hit PLAYER_1");
                        }
                        inTarget[0] = true;
                    }
                    if (bulletCircle.getBoundsInParent().intersects(playerPane.getBoundsInParent())
                            || bulletCircle.getBoundsInParent().intersects(enemyPane.getBoundsInParent())) {
                        bulletCircle.setVisible(false);
                    }

                }
            }));

            timeline.setCycleCount(500);
            timeline.play();
            socketClient.sendMessage("shot " + playerNumber);
        }
    };

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        registrButton.setDisable(true);
        playerNameTextField.setDisable(true);
        playerCircle.setVisible(false);
        enemyCircle.setVisible(false);
        playerPane.setVisible(false);
        enemyPane.setVisible(false);
        serverLogsTextArea.setDisable(true);

        connectButton.setOnAction(event -> {
            socketClient = new SocketClient("localhost", 7777, this);
            connectButton.setDisable(true);
            registrButton.setDisable(false);
            playerNameTextField.setDisable(false);
        });
        registrButton.setOnAction(event -> {
            playerNameLabel.setText(playerNameTextField.getText());
            socketClient.sendMessage("start " + playerNameTextField.getText());
            playerNameTextField.setEditable(false);
            playerNameTextField.setDisable(true);
            registrButton.setDisable(true);
            playerCircle.setVisible(true);
            enemyCircle.setVisible(true);
            playerPane.setVisible(true);
            enemyPane.setVisible(true);
            registrButton.getScene().getRoot().requestFocus();
        });

    }
}

