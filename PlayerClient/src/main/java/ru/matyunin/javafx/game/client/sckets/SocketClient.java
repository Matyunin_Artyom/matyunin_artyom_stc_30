package ru.matyunin.javafx.game.client.sckets;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import ru.matyunin.javafx.game.client.controller.Controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketClient {
    private Socket client;
    private Controller controller;

    private PrintWriter toServer;
    private BufferedReader fromServer;

    public SocketClient(String host, int port, Controller controller) {
        try {
            client = new Socket(host, port);
            this.controller = controller;
            toServer = new PrintWriter(client.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(client.getInputStream()));
            new Thread(receiverMessageTask).start();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void sendMessage(String message) {
        toServer.println(message);
    }

    private Runnable receiverMessageTask = () -> {
        while (true) {
            try {
                String messageFromServer = fromServer.readLine();
                if (messageFromServer != null) {

                    if (messageFromServer.startsWith("PLAYER")) {
                        controller.setPlayerNumber(messageFromServer);
                    }
                    if (messageFromServer.contains("move")) {
                        String[] parsedMessage = messageFromServer.split(" ");
                        if (!parsedMessage[2].equals(controller.getPlayerNumber())) {
                            if (parsedMessage[1].equals("RIGHT")) {
                                controller.getEnemyCircle().setCenterX(controller.getEnemyCircle().getCenterX() + 10);
                            } else {
                                controller.getEnemyCircle().setCenterX(controller.getEnemyCircle().getCenterX() - 10);
                            }
                        }
                    }
                    if (messageFromServer.contains("shot")) {
                        String[] parsedMessage = messageFromServer.split(" ");
                        if (!parsedMessage[1].equals(controller.getPlayerNumber())) {
                            Platform.runLater(() -> {
                                Circle bulletCircle = new Circle();
                                bulletCircle.setRadius(5);
                                bulletCircle.setCenterX(controller.getEnemyCircle().getCenterX() + controller.getEnemyCircle().getLayoutX());
                                bulletCircle.setCenterY(controller.getEnemyCircle().getCenterY() + controller.getEnemyCircle().getLayoutY());
                                bulletCircle.setFill(Color.ORANGE);
                                controller.getPane().getChildren().add(bulletCircle);

                                Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0.0025), animation -> {
                                    bulletCircle.setCenterY(bulletCircle.getCenterY() + 1);

                                    if (bulletCircle.getBoundsInParent().intersects(controller.getPlayerCircle().getBoundsInParent())) {
                                        bulletCircle.setVisible(false);
                                        controller.getPane().getChildren().remove(bulletCircle);
                                    }
                                    if (bulletCircle.getBoundsInParent().intersects(controller.getPlayerPane().getBoundsInParent())
                                            || bulletCircle.getBoundsInParent().intersects(controller.getEnemyPane().getBoundsInParent())) {
                                        bulletCircle.setVisible(false);
                                        controller.getPane().getChildren().remove(bulletCircle);
                                    }

                                }));
                                timeline.setCycleCount(500);
                                timeline.play();
                            });

                        }
                    }
                    if (messageFromServer.contains("hit")) {
                        String[] parsedMessage = messageFromServer.split(" ");
                        String player = parsedMessage[1];
                        if (controller.getPlayerNumber().equals(player)) {
                            Platform.runLater(() ->
                                    controller.getPlayerHealthLabel().setText(String.valueOf(Integer.parseInt(controller.getPlayerHealthLabel().getText()) - 1))
                            );
                        } else {
                            Platform.runLater(() ->
                                    controller.getEnemyHealthLabel().setText(String.valueOf(Integer.parseInt(controller.getEnemyHealthLabel().getText()) - 1)));
                        }
                        if (controller.getEnemyHealthLabel().getText().equals("0")) {
                            sendMessage("GameOver " + controller.getPlayerNumber() + " " + controller.getPlayerHealthLabel().getText());
                            client.close();
                        }
                    }
                    controller.getServerLogsTextArea().appendText(messageFromServer + "\n");
                }
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    };


}
