package ru.matyunin;

/**
 * 07.11.2020
 * 24. Associative Arrays
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class HashMapImpl<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 16;

    private MapEntry<K, V>[] entries = new MapEntry[DEFAULT_SIZE];

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    // TODO: предусмотреть ЗАМЕНУ ЗНАЧЕНИЯ
    // put("Марсель", 27);
    // put("Марсель", 28);
    // у меня сохранятся оба значения, вам нужно сделать, чтобы хранилось только одно
    @Override
    public void put(K key, V value) {
        MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
        if (key == null) {
            System.out.println("Key <NULL> cannot be added");
        } else {
            int index = key.hashCode() & (entries.length - 1);
            if (entries[index] == null || entries[index].key.equals(key)) {
                entries[index] = newMapEntry;
            } else {
                MapEntry<K, V> current = entries[index];
                if (current.next == null) {
                    current.next = newMapEntry;
                }
                while (current.next != null) {
                    current = current.next;
                    if (current.key.equals(key)) {
                        current.value = value;
                        break;
                    } else if (current.next == null) {
                        current.next = newMapEntry;
                    }
                }
            }
        }
    }

    // TODO: реализовать
    @Override
    public V get(K key) {
        if (key == null) {
            return null;
        }
        int index = key.hashCode() & (entries.length - 1);
        if (entries[index] == null) {
            return null;
        } else if (entries[index].key.equals(key)) {
            return entries[index].value;
        } else if (entries[index] != null) {
            MapEntry<K, V> current = entries[index];
            //current = current.next;
            while (current.next != null) {
                current = current.next;
                if (current.key.equals(key)) {
                    return current.value;
                }
            }
        }
        return null;
    }
}
