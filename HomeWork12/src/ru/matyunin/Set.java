package ru.matyunin;

/**
 * 07.11.2020
 * 24. Associative Arrays
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// коллекция, которая гарантирует уникальность своих значений
    // TODO: реализовать класс HashSet, который будет использовать HashMap
public interface Set<V> {
    void add(V value);
    boolean contains(V value);
}
