package ru.matyunin;

public class SetMain {
    public static void main(String[] args) {
        Set<String> stringSet = new HashSetString<>();
        stringSet.add("String");
        stringSet.add("String1");
        stringSet.add("String1");
        stringSet.add("String2");
        stringSet.add("String3");
        stringSet.add("String4");
        stringSet.add("String4");
        stringSet.add("String5");
        int i = 0;

        System.out.println(stringSet.contains("String"));
        System.out.println(stringSet.contains("String1"));
        System.out.println(stringSet.contains("String2"));
        System.out.println(stringSet.contains("String3"));
        System.out.println(stringSet.contains("String4"));
        System.out.println(stringSet.contains("String6"));
        System.out.println(stringSet.contains("String7"));
        System.out.println(stringSet.contains("String8"));

    }
}
