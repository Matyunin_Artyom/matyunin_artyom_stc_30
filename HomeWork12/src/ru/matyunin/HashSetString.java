package ru.matyunin;

public class HashSetString<V> implements Set<V> {
    private static final Object PRESENT = new Object();
    private transient HashMapImpl<V,Object> map;
    public HashSetString(){
        map = new HashMapImpl<>();
    }

    @Override
    public void add(V value) {
        map.put(value, PRESENT);
    }

    @Override
    public boolean contains(V value) {
        if(map.get(value)!=null){
            return true;
        }
        else{
            return false;
        }

    }
}
