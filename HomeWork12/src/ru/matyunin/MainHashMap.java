package ru.matyunin;

/**
 * 07.11.2020
 * 24. Associative Arrays
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainHashMap {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMapImpl<>();
        map.put(null, null);
        map.put("Марсель", 26);
        map.put("Марсельm", 26);
        map.put("Марсель", 35);
        map.put("Денис", 30);
        map.put("Денис", 38);
        map.put("Илья", 28);
        map.put("Неля", 18);
        map.put("Неля", 74);
        map.put("Катерина", 23);
        map.put("Полина", 18);
        map.put("Регина", 75);
        map.put("Максим", 18);
        map.put("Сергей", 18);
        map.put("Иван", 18);
        map.put("Виктор", 21);
        map.put("Виктор Александрович", 25);
        map.put("Виктор Александрович", 48);
        map.put("Кристина", null);

        System.out.println(map.get("Марсель"));
        System.out.println(map.get("Денис"));
        System.out.println(map.get("Илья"));
        System.out.println(map.get("Неля"));
        System.out.println(map.get("Катерина"));
        System.out.println(map.get("Полина"));
        System.out.println(map.get("Регина"));
        System.out.println(map.get("Максим"));
        System.out.println(map.get("Сергей"));
        System.out.println(map.get("Иван"));
        System.out.println(map.get("Виктор Александрович"));
        System.out.println(map.get("Виктор"));
        System.out.println(map.get("Афанасий"));
        System.out.println(map.get("Кристина"));
    }
}
