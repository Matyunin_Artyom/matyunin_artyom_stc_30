package ru.matyunin;

/**
 * 07.11.2020
 * 24. Associative Arrays
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface Map<K, V> {
    // положить значение value, по ключу key
    void put(K key, V value);
    // получить значение по ключу
    V get(K key);
}
