package ru.matyunin;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class TestMain {

    public static String[] quickStringSort(String[] strings, int leftIndex, int rightIndex) {
        {
            String supportElement;
            int leftStartIndex = leftIndex;
            int rightStartIndex = rightIndex;
            supportElement = strings[leftIndex];
            while (leftIndex < rightIndex) {
                while ((strings[rightIndex].compareTo(supportElement) >= 0) && (leftIndex < rightIndex))
                    rightIndex--;
                if (leftIndex != rightIndex) {
                    strings[leftIndex] = strings[rightIndex];
                    leftIndex++;
                }
                while ((strings[leftIndex].compareTo(supportElement) <= 0) && (leftIndex < rightIndex))
                    leftIndex++;
                if (leftIndex != rightIndex) {
                    strings[rightIndex] = strings[leftIndex];
                    rightIndex--;
                }
            }
            strings[leftIndex] = supportElement;
            int currentIndex = leftIndex;
            leftIndex = leftStartIndex;
            rightIndex = rightStartIndex;
            if (leftIndex < currentIndex)
                quickStringSort(strings, leftIndex, currentIndex - 1);
            if (rightIndex > currentIndex)
                quickStringSort(strings, currentIndex + 1, rightIndex);
        }
        return strings;
    }

    public static String[] getArray() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("text.txt"));
            String[] current = bufferedReader.readLine().split(" ");
            return current;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    public static void main(String[] args) {

        System.out.println(Arrays.toString(getArray()));
        System.out.println(Arrays.toString(quickStringSort(getArray(), 0, getArray().length - 1)));

    }
}
