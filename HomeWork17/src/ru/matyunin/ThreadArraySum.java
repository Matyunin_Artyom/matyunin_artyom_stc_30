package ru.matyunin;

public class ThreadArraySum extends Thread {

    private int[] arraySum;
    private int a;
    private int b;
    private int result;

    public int getResult() {
        return result;
    }

    public ThreadArraySum(int[] arraySum, int a, int b) {
        this.arraySum = arraySum;
        this.a = a;
        this.b = b;
    }

    @Override
    public void run() {
        result = 0;
        for (int i = a; i < b; i++) {
            result += this.arraySum[i];
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
            System.out.println("Сейчас считает поток: " + currentThread().getName());
        }
        System.out.println("Сумма, посчитанная потоком: " + "'" + this.getName() + "'" + " = " + result);
    }
}
