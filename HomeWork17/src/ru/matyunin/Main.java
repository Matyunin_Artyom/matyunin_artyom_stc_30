package ru.matyunin;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        System.out.println("Введите число. Все элементы массива будут равны этому числу. Размер массива будет выбран случайно, но не более 100 элементов");
        int arrayElement = scanner.nextInt();
        int arraySize = random.nextInt(100);
        scanner.nextLine();

        int[] threadArray = new int[arraySize];
        for (int i = 0; i < threadArray.length; i++) {
            threadArray[i] = arrayElement;
        }

        ThreadArraySum threadArraySum1 = new ThreadArraySum(threadArray, 0, threadArray.length / 3);
        ThreadArraySum threadArraySum2 = new ThreadArraySum(threadArray, threadArray.length / 3, (threadArray.length / 3) * 2);
        ThreadArraySum threadArraySum3 = new ThreadArraySum(threadArray, (threadArray.length / 3) * 2, threadArray.length);

        System.out.println("Создан массив элементов " + "<" + arrayElement + ">" + " размером " + "<" + threadArray.length + ">");
        System.out.println("Он будет поделен на 3 части. Для подсчета сумм элементов этих частей нахмите Enter");
        scanner.nextLine();

        threadArraySum1.start();
        threadArraySum2.start();
        threadArraySum3.start();

        try {
            threadArraySum1.join();
            threadArraySum2.join();
            threadArraySum3.join();
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }

        int result = threadArraySum1.getResult() + threadArraySum2.getResult() + threadArraySum3.getResult();
        System.out.println("Сумма всех потоков = " + result);

    }
}
