package ru.matyunin;

public class RemoteController {

    private TV tv;
    private int button;

    public int getTvChannel() {
        return tvChannel;
    }

    public void setTvChannel(int tvChanel) {
        this.tvChannel = tvChanel;
    }

    int tvChannel;

    public TV getTv() {

        return tv;
    }

    public void setTv(TV tv) {

        this.tv = tv;
    }

    public int getButton() {
        return button;
    }

    public void setButton(int button) {
        this.button = button;
    }

    public void configTV(TV tv) {
        this.tv = tv;
        this.tv.configRemoteController(this);
    }

    public int buttonClick(int button) {
        tvChannel = this.button;
        return tvChannel;
    }

    public void startTV() {
        this.tv.printListChannel();
    }
}
