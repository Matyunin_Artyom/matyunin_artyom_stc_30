package ru.matyunin;

import java.util.Random;

public class Channel {
    final String nameChannel;
    private Program[] programs;
    private int countProgram;
    private TV tv;

    public Channel(String nameChannel) {

        this.nameChannel = nameChannel;
        this.programs = new Program[5];
    }

    public String getNameChannel() {
        return nameChannel;
    }

    public int getCountProgram() {
        return countProgram;
    }

    public void setCountProgram(int countProgram) {
        this.countProgram = countProgram;
    }

    public void listProgram(Program program) {
        this.programs[countProgram++] = program;
        //this.countProgram++;
    }

    public void broadcastChannel(TV tv) {
        this.tv = tv;
        this.tv.listChannel(this);
    }

    public void printListProgram() {
        Random random = new Random();
        int num = random.nextInt(4);
        System.out.println("now on this channel: " + programs[num].getNameProgram());
    }
}
