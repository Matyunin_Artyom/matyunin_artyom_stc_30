package ru.matyunin;

public class Program {

    private Channel channel;
    private String nameProgram;

    public Program(String nameProgram) {

        this.nameProgram = nameProgram;
    }

    public String getNameProgram() {

        return nameProgram;
    }

    public void setNameProgram(String nameProgram) {

        this.nameProgram = nameProgram;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public void broadcastProgram(Channel channel) {
        this.channel = channel;
        this.channel.listProgram(this);
    }
}
