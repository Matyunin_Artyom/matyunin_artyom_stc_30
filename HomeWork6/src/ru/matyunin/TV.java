package ru.matyunin;

public class TV {

    private String nameTv;
    private RemoteController controller;
    private int channelCount;
    private Channel[] channelPosition;


    public TV(String nameTv) {
        this.nameTv = nameTv;
        this.channelPosition = new Channel[5];
    }

    public RemoteController getController() {
        return controller;
    }

    public void listChannel(Channel channel) {
        this.channelPosition[channelCount] = channel;
        this.channelCount++;
    }

    public void configRemoteController(RemoteController remoteController) {
        this.controller = remoteController;
    }

    public void printListChannel() {
        System.out.println("Name tv: " + nameTv);
        System.out.println("Channel: " + channelPosition[controller.getButton()].getNameChannel());
        this.channelPosition[controller.getButton()].printListProgram();
    }

}