package ru.matyunin;

import java.util.Scanner;


public class MainTVremote {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter number of channel(from 1 to 5):");
        int intChannelNumber = scanner.nextInt() - 1;

        TV tv = new TV("Рассвет");
        RemoteController remotecontroller = new RemoteController();
        tv.configRemoteController(remotecontroller);
        remotecontroller.configTV(tv);
        remotecontroller.setButton(intChannelNumber);

        Channel first = new Channel("KARUSEL");
        Channel second = new Channel("RUSSIA-1");
        Channel third = new Channel("SPORT");
        Channel fourth = new Channel("TNT");
        Channel fifth = new Channel("FIRST CHANNEL");
        first.broadcastChannel(tv);
        second.broadcastChannel(tv);
        third.broadcastChannel(tv);
        fourth.broadcastChannel(tv);
        fifth.broadcastChannel(tv);

        Program fixiki = new Program("Fixiki");
        Program smeshariki = new Program("Smesharyki");
        Program mimimishki = new Program("Mi-mi-mishki");
        Program derevyashky = new Program("Derevyashki");
        Program trykota = new Program("Three cats");
        Program krivoezerkalo = new Program("Crooked mirror");
        Program malahov = new Program("Malakhov");
        Program mylo1 = new Program("Some boring movie");
        Program mylo2 = new Program("Some very boring movie");
        Program mylo3 = new Program("Soap opera");
        Program football = new Program("Football");
        Program basketBall = new Program("BasketBall");
        Program hockey = new Program("Hockey");
        Program tennis = new Program("Tennis");
        Program handball = new Program("Handball");
        Program comedi = new Program("Comedi club");
        Program comediwoomen = new Program("Comedi Woomen");
        Program sashatanya = new Program("Real guys");
        Program univer = new Program("Univer");
        Program interny = new Program("Interny");
        Program vremya = new Program("Time");
        Program urgant = new Program("Evening Urgant");
        Program polechudes = new Program("Field of Dreams");
        Program serial = new Program("Moronic political talk show");
        Program luchshevseh = new Program("The best");

        fixiki.broadcastProgram(first);
        smeshariki.broadcastProgram(first);
        mimimishki.broadcastProgram(first);
        derevyashky.broadcastProgram(first);
        trykota.broadcastProgram(first);
        krivoezerkalo.broadcastProgram(second);
        malahov.broadcastProgram(second);
        mylo1.broadcastProgram(second);
        mylo2.broadcastProgram(second);
        mylo3.broadcastProgram(second);
        football.broadcastProgram(third);
        basketBall.broadcastProgram(third);
        hockey.broadcastProgram(third);
        tennis.broadcastProgram(third);
        handball.broadcastProgram(third);
        comedi.broadcastProgram(fourth);
        comediwoomen.broadcastProgram(fourth);
        sashatanya.broadcastProgram(fourth);
        univer.broadcastProgram(fourth);
        interny.broadcastProgram(fourth);
        vremya.broadcastProgram(fifth);
        urgant.broadcastProgram(fifth);
        polechudes.broadcastProgram(fifth);
        serial.broadcastProgram(fifth);
        luchshevseh.broadcastProgram(fifth);

        remotecontroller.startTV();
    }
}
