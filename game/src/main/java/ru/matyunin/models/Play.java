package ru.matyunin.models;

import lombok.*;

import java.sql.Date;
import java.sql.Time;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Play {
    private int playId;
    private String playDate;
    private int firstPlayer;
    private int secondPlayer;
    private int playFirstPlayerShotsCount;
    private int playSecondPlayerShotsCount;
    private String playStart;
    private String playStop;
    private int playFirstPlayerPoints;
    private int playSecondPlayerPoints;

}

