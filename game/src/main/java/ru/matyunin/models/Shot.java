package ru.matyunin.models;

import lombok.*;

import java.sql.Time;
import java.util.Optional;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Shot {
    private int shotId;
    private int shotPlayer;
    private int shotInPlay;
    private boolean shotOnTarget;
    private String shotTime;
    private int shotTargetPlayer;

}
