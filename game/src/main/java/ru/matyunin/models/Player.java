package ru.matyunin.models;

import lombok.*;


@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Player {
    private int playerId;
    private String playerIpAddress;
    private String playerName;
    private int playerMaxPoint;
    private int playerWinCount;
    private int playerFailCount;

    public Player(String name) {
        this.playerName = name;
    }
}
