package ru.matyunin.server;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.matyunin.models.Play;
import ru.matyunin.models.Player;
import ru.matyunin.repositories.PlayersRepository;
import ru.matyunin.repositories.PlayersRepositoryImpl;
import ru.matyunin.service.PlayService;
import ru.matyunin.service.PlayServiceImpl;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Date;

public class PlayServer {

    private Socket firstPlayer;
    private Socket secondPlayer;

    private PlayerThread firstPlayerThread;
    private PlayerThread secondPlayerThread;
    private PlayService playService;
    private int playId;
    private boolean isGameStarted = false;

    public PlayServer(PlayService playService){
        this.playService = playService;
    }

    public void start(int port) {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            while (!isGameStarted) {
                if (firstPlayer == null) {
                    firstPlayer = serverSocket.accept();
                    System.out.println("Первый игрок подключен");
                    firstPlayerThread = new PlayerThread(firstPlayer);
                    firstPlayerThread.playerNumber = "PLAYER_1";
                    firstPlayerThread.start();
                } else {
                    secondPlayer = serverSocket.accept();
                    System.out.println("Второй игрок подключен");
                    secondPlayerThread = new PlayerThread(secondPlayer);
                    secondPlayerThread.playerNumber = "PLAYER_2";
                    secondPlayerThread.start();
                    isGameStarted = true;
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private class PlayerThread extends Thread {
        private Socket player;

        private BufferedReader fromClient;
        private PrintWriter toClient;
        private String playerName;

        private String playerNumber;

        public PlayerThread(Socket player) {
            this.player = player;
            try {
                this.fromClient = new BufferedReader(new InputStreamReader(player.getInputStream()));
                this.toClient = new PrintWriter(player.getOutputStream(), true);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }

        }

        @Override
        public void run() {
            while (true) {
                try {
                    String messageFromClient = fromClient.readLine();
                    if (messageFromClient != null) {
                        System.out.println("Получили сообщение от: " + player.getInetAddress().getHostAddress()
                                + ": " + player.getPort() + " - " + messageFromClient);
                        String command[] = messageFromClient.split(" ");

                        if (command[0].equals("start")) {
                            this.playerName = command[1];
                            playService.createOrUpdatePlayer(this.playerName, player.getInetAddress().getHostAddress());
                            if (firstPlayerThread.playerName != null && secondPlayerThread.playerName != null) {
                                firstPlayerThread.toClient.println("PLAYER_1");
                                secondPlayerThread.toClient.println("PLAYER_2");
                                playId = playService.startPlay(firstPlayerThread.playerName, secondPlayerThread.playerName);
                            }

                        } else if (command[0].equals("shot")) {
                            if (command[1].equals("PLAYER_1")) {
                                playService.shot(playId, firstPlayerThread.playerName, secondPlayerThread.playerName);
                            } else if (command[1].equals("PLAYER_2")) {
                                playService.shot(playId, secondPlayerThread.playerName, firstPlayerThread.playerName);
                            }
                        } else if(command[0].equals("GameOver")){
                            if(command[1].equals("PLAYER_1")){
                                playService.stopPlay(playId, firstPlayerThread.playerName, secondPlayerThread.playerName, Integer.parseInt(command[2]));
                            }
                            else if(command[1].equals("PLAYER_2")){
                                playService.stopPlay(playId, secondPlayerThread.playerName,  firstPlayerThread.playerName, Integer.parseInt(command[2]));
                            }
                        }

                        firstPlayerThread.toClient.println(messageFromClient);
                        secondPlayerThread.toClient.println(messageFromClient);
                    }

                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }


}
