package ru.matyunin.repositories;

import ru.matyunin.models.Play;
import ru.matyunin.models.Player;

import java.util.Optional;

public interface PlayersRepository extends CrudRepository<Player, Integer> {
    Optional<Player> findByName(String name);
    void update(Player player);
    Optional<Player> findById(Integer id);



}
