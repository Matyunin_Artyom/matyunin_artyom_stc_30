package ru.matyunin.repositories;

import ru.matyunin.models.Play;

import java.util.Optional;

public interface PlaysRepository extends CrudRepository<Play, Integer> {
    void update(Play play);
    Optional<Play> findById(Integer id);
}
