package ru.matyunin.repositories;

import ru.matyunin.models.Shot;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;

public class ShotsRepositoryImpl implements ShotsRepository {

    private static final String SQL_INSERT = "insert into shot (shot_player, shot_in_play, shot_on_target, shot_time, shot_target_player) values (?, ?, ?, ?, ?)";

    private final DataSource dataSource;

    public ShotsRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private final RowMapper<Shot> shotRowMapper = row -> Shot.builder()
            .shotId(row.getInt("shot_id"))
            .shotPlayer(row.getInt("shot_player"))
            .shotInPlay(row.getInt("shot_in_play"))
            .shotOnTarget(row.getBoolean("shot_on_target"))
            .shotTime(row.getString("shot_time"))
            .build();

    @Override
    public void save(Shot entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);) {
            statement.setInt(1, entity.getShotPlayer());
            statement.setInt(2, entity.getShotInPlay());
            statement.setBoolean(3, entity.isShotOnTarget());
            statement.setString(4, entity.getShotTime());
            statement.setInt(5, entity.getShotTargetPlayer());
            int insertedRowsCount = statement.executeUpdate();
            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save entity");
            }

            ResultSet generatedIds = statement.getGeneratedKeys();

            if (generatedIds.next()) {
                int generatedId = generatedIds.getInt("shot_id");
                entity.setShotId(generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }

            generatedIds.close();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

}

