package ru.matyunin.repositories;

import ru.matyunin.models.Play;
import ru.matyunin.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;

public class PlaysRepositoryImpl implements PlaysRepository {

    private static final String SQL_INSERT = "insert into play (play_date, " +
            "play_first_player, " +
            "play_second_player, " +
            "play_f_player_shots_count, " +
            "play_s_player_shots_count, " +
            "play_start, " +
            "play_stop, " +
            "play_f_player_points, " +
            "play_s_player_points) " +
            "values (?, ?, ?, ?, ?, ?, ?, ?, ?) ";

    private static final String SQL_UPDATE = "update play set " +
            "play_date = ?, "+
            "play_first_player = ?, " +
            "play_second_player = ?, " +
            "play_f_player_shots_count = ?, " +
            "play_s_player_shots_count = ?, " +
            "play_start = ?, " +
            "play_stop = ?, " +
            "play_f_player_points = ?, " +
            "play_s_player_points = ? " +
            "where play_id = ?";

    private static final String SQL_FIND_BY_ID = "select * from play where play_id = ?";

    private final DataSource dataSource;

    public PlaysRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private final RowMapper<Play> playRowMapper = row -> Play.builder()
            .playId(row.getInt("play_id"))
            .playDate(row.getString("play_date"))
            .firstPlayer(row.getInt("play_first_player"))
            .secondPlayer(row.getInt("play_second_player"))
            .playFirstPlayerShotsCount(row.getInt("play_f_player_shots_count"))
            .playSecondPlayerShotsCount(row.getInt("play_s_player_shots_count"))
            .playStart(row.getString("play_start"))
            .playStop(row.getString("play_stop"))
            .playFirstPlayerPoints(row.getInt("play_f_player_points"))
            .playSecondPlayerPoints(row.getInt("play_s_player_points"))
            .build();

    @Override
    public void save(Play play) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);) {
            statement.setString(1, play.getPlayDate());
            statement.setInt(2, play.getFirstPlayer());
            statement.setInt(3, play.getSecondPlayer());
            statement.setInt(4, play.getPlayFirstPlayerShotsCount());
            statement.setInt(5, play.getPlaySecondPlayerShotsCount());
            statement.setString(6, play.getPlayStart());
            statement.setString(7, play.getPlayStop());
            statement.setInt(8, play.getPlayFirstPlayerPoints());
            statement.setInt(9, play.getPlaySecondPlayerPoints());

            int insertedRowsCount = statement.executeUpdate();
            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save entity");
            }
            ResultSet generatedIds = statement.getGeneratedKeys();
            if (generatedIds.next()) {
                int generatedId = generatedIds.getInt("play_id");
                play.setPlayId(generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }

            generatedIds.close();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Play play) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setString(1, play.getPlayDate());
            statement.setInt(2, play.getFirstPlayer());
            statement.setInt(3, play.getSecondPlayer());
            statement.setInt(4, play.getPlayFirstPlayerShotsCount());
            statement.setInt(5, play.getPlaySecondPlayerShotsCount());
            statement.setString(6, play.getPlayStart());
            statement.setString(7, play.getPlayStop());
            statement.setInt(8, play.getPlayFirstPlayerPoints());
            statement.setInt(9, play.getPlaySecondPlayerPoints());
            statement.setInt(10, play.getPlayId());

            int insertedRowsCount = statement.executeUpdate();
            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot update entity");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public Optional<Play> findById(Integer playId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID)) {
            statement.setInt(1, playId);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return Optional.of(playRowMapper.mapRow(result));
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
