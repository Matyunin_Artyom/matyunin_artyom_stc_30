package ru.matyunin.repositories;

import lombok.Data;
import ru.matyunin.models.Player;
import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;
@Data

public class PlayersRepositoryImpl implements PlayersRepository {

    private static final String SQL_INSERT = "insert into player (player_ip_address, " +
            "player_name, " +
            "player_max_point, " +
            "player_win_count, " +
            "player_fail_count )" +
            "values (?, ?, ?, ?, ?) ";

    private static final String SQL_FIND_BY_ID = "select * from player where player_id = ?";
    private static final String SQL_FIND_BY_PLAYER_NAME = "select * from player where player_name = ?";
    private static final String SQL_UPDATE = "update player set " +
             "player_ip_address = ?, " +
              "player_name = ?, " +
            "player_max_point = ?, " +
            "player_win_count = ?, " +
            "player_fail_count = ? " +
            "where player_id = ?";

    private DataSource dataSource;

    public PlayersRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private final RowMapper<Player> playerRowMapper = row -> Player.builder()
            .playerId(row.getInt("player_id"))
            .playerIpAddress(row.getString("player_ip_address"))
            .playerName(row.getString("player_name"))
            .playerMaxPoint(row.getInt("player_max_point"))
            .playerWinCount(row.getInt("player_win_count"))
            .playerFailCount(row.getInt("player_fail_count"))
            .build();

    @Override
    public void update(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setString(1, player.getPlayerIpAddress());
            statement.setString(2, player.getPlayerName());
            statement.setInt(3, player.getPlayerMaxPoint());
            statement.setInt(4, player.getPlayerWinCount());
            statement.setInt(5, player.getPlayerFailCount());
            statement.setInt(6, player.getPlayerId());


            int insertedRowsCount = statement.executeUpdate();
            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot update entity");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public Optional<Player> findById(Integer playerId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID)) {

            statement.setInt(1, playerId);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return Optional.of(playerRowMapper.mapRow(result));
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public Optional<Player> findByName(String name) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_PLAYER_NAME)) {

            statement.setString(1, name);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return Optional.of(playerRowMapper.mapRow(result));
            } else {

                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void save(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);) {
            statement.setString(1, player.getPlayerIpAddress());
            statement.setString(2, player.getPlayerName());
            statement.setInt(3, player.getPlayerMaxPoint());
            statement.setInt(4, player.getPlayerWinCount());
            statement.setInt(5, player.getPlayerFailCount());

            int insertedRowsCount = statement.executeUpdate();
            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save entity");
            }
            ResultSet generatedIds = statement.getGeneratedKeys();
            if (generatedIds.next()) {
                int generatedId = generatedIds.getInt("player_id");
                player.setPlayerId(generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }

            generatedIds.close();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }
}
