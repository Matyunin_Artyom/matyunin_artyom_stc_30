package ru.matyunin.service;

import ru.matyunin.models.Play;
import ru.matyunin.models.Player;
import ru.matyunin.models.Shot;

public interface PlayService {
    void createOrUpdatePlayer(String playerName, String playerIp);

    Integer startPlay (String firstPlayerName, String secondPlayerName);

    void stopPlay(int playId, String playerWinName, String playerFailName, int playerPoints);

    void shot(int playId, String shooterName, String targetName);


}
