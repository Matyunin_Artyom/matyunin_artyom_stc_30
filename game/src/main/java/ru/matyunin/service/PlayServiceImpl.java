package ru.matyunin.service;

import ru.matyunin.models.Play;
import ru.matyunin.models.Player;
import ru.matyunin.models.Shot;
import ru.matyunin.repositories.*;

import java.time.LocalDateTime;
import java.util.Optional;

public class PlayServiceImpl implements PlayService {

    private final ShotsRepository shotsRepository;
    private final PlayersRepository playersRepository;
    private final PlaysRepository playsRepository;

    public PlayServiceImpl(ShotsRepository shotsRepository, PlayersRepository playersRepository, PlaysRepository playsRepository) {
        this.shotsRepository = shotsRepository;
        this.playersRepository = playersRepository;
        this.playsRepository = playsRepository;
    }

    @Override
    public void createOrUpdatePlayer(String playerName, String playerIp) {
        //ищем игорка с таким именем в БД
        Optional<Player> findPlayer = playersRepository.findByName(playerName);
        //еси он есть. то обновляем ему IP
        if (findPlayer.isPresent()) {
            findPlayer.get().setPlayerIpAddress(playerIp);
            playersRepository.update(findPlayer.get());
    } else {
            //если его нет, то добавляем в БД
            Player dntFindPlayer = new Player()
                    .builder()
                    .playerName(playerName)
                    .playerIpAddress(playerIp)
                    .build();
            playersRepository.save(dntFindPlayer);
            }
    }

    @Override
    public Integer startPlay(String firstPlayerName, String secondPlayerName) {
        Optional<Player> firstPlayer = playersRepository.findByName(firstPlayerName);
        Optional<Player> secondPlayer = playersRepository.findByName(secondPlayerName);
        Play play = new Play().builder()
                .firstPlayer(firstPlayer.get().getPlayerId())
                .secondPlayer(secondPlayer.get().getPlayerId())
                .playDate(LocalDateTime.now().toString())
                .playFirstPlayerPoints(0)
                .playSecondPlayerPoints(0)
                .playStart(LocalDateTime.now().toString())
                .playStop(LocalDateTime.now().toString())
                .playFirstPlayerShotsCount(0)
                .playSecondPlayerShotsCount(0)
                .build();
       playsRepository.save(play);
        return play.getPlayId();

    }

    @Override
    public void stopPlay(int playId, String winPlayerName, String failPlayerName, int playerPoints) {
        Optional<Player> findWinPlayer = playersRepository.findByName(winPlayerName);
        Optional<Player> findFailPlayer = playersRepository.findByName(failPlayerName);
        Optional<Play> findPlay = playsRepository.findById(playId);
        if(findWinPlayer.isPresent()){
            findWinPlayer.get().setPlayerWinCount(findWinPlayer.get().getPlayerWinCount() + 1);
            if(findWinPlayer.get().getPlayerMaxPoint() < playerPoints) {
                findWinPlayer.get().setPlayerMaxPoint(playerPoints);
                playersRepository.update(findWinPlayer.get());
            }
        }
        if (findFailPlayer.isPresent()){
            findFailPlayer.get().setPlayerFailCount(findFailPlayer.get().getPlayerFailCount() + 1);
            playersRepository.update(findFailPlayer.get());
        }
        if (findPlay.isPresent()){
           if(findPlay.get().getFirstPlayer() == findWinPlayer.get().getPlayerId()){
               findPlay.get().setPlayFirstPlayerPoints(playerPoints);
           } else {
               findPlay.get().setPlaySecondPlayerPoints(playerPoints);
           }
           findPlay.get().setPlayStop(LocalDateTime.now().toString());
           playsRepository.update(findPlay.get());
        }

    }

    @Override
    public void shot(int playId, String shooterName, String targetName) {
        Optional<Player> shooter = playersRepository.findByName(shooterName);
        Optional<Player> target = playersRepository.findByName(targetName);
        Optional<Play> play = playsRepository.findById(playId);
        if(shooter.isPresent()&target.isPresent()&play.isPresent()) {
            Shot shot = new Shot().builder()
                    .shotInPlay(playId)
                    .shotOnTarget(false)
                    .shotPlayer(shooter.get().getPlayerId())
                    .shotTargetPlayer(target.get().getPlayerId())
                    .shotTime(LocalDateTime.now().toString())
                    .build();

            if (play.get().getFirstPlayer() == shooter.get().getPlayerId()) {
                play.get().setPlayFirstPlayerShotsCount(play.get().getPlayFirstPlayerShotsCount() + 1);

            } else if (play.get().getSecondPlayer() == shooter.get().getPlayerId()) {
                play.get().setPlaySecondPlayerShotsCount(play.get().getPlaySecondPlayerShotsCount() + 1);
            }
            shotsRepository.save(shot);
            playsRepository.update(play.get());
        }
    }
}
