package ru.matyunin;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.matyunin.repositories.*;
import ru.matyunin.server.PlayServer;
import ru.matyunin.service.PlayService;
import ru.matyunin.service.PlayServiceImpl;

import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/db_game");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("2015@Yura");
        hikariConfig.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(hikariConfig);

        PlayService playService = new PlayServiceImpl(new ShotsRepositoryImpl(dataSource), new PlayersRepositoryImpl(dataSource), new PlaysRepositoryImpl(dataSource));
        PlayServer playServer = new PlayServer(playService);
        playServer.start(7777);


    }
}
