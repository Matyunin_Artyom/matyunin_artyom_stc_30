package ru.matyunin.stack;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

public class Main {

    public static boolean isMatch(char openedBracket, char closedBracket) {
        return openedBracket == '(' && closedBracket == ')' ||
                openedBracket == '<' && closedBracket == '>' ||
                openedBracket == '[' && closedBracket == ']' ||
                openedBracket == '{' && closedBracket == '}';
    }

    public static boolean isOpened(char bracket) {
        return "{[(<".indexOf(bracket) != -1;
    }

    public static boolean isClosed(char bracket) {
        return "}])>".indexOf(bracket) != -1;
    }

    public static void main(String[] args) {
	    // [{<<>>}()()()]

        String input =  "({<<>>}()()(){}{}{{{}}})";

        char brackets[] = input.toCharArray();

        Deque<Character> stack = new LinkedList<>();

        for (int i = 0; i < brackets.length; i++) {
            System.out.println(Arrays.toString(stack.toArray()));
            char currentBracket = brackets[i];

            if (isOpened(currentBracket)) {
                stack.push(currentBracket);
            } else if (isClosed(currentBracket) && !stack.isEmpty()) {
                char bracketFromStack = stack.pop();
                if (!isMatch(bracketFromStack, currentBracket)) {
                    System.exit(-1);
                }
            } else {
                System.exit(-1);
            }
        }

        if (stack.isEmpty()) {
            System.out.println("ALL OK");
        } else {
            System.exit(-1);
        }
    }
}
