package ru.matyunin.graphasadjacencylist;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Objects;

public class GraphAdj {

    private ArrayList<LinkedList<Integer>> adj_list;
    private static int count;

    public static int getCount() {
        return count;
    }

    public GraphAdj() {
        this.adj_list = new ArrayList<>();
    }

    public static void setCount(int count) {
        GraphAdj.count = count;
    }

    public ArrayList<LinkedList<Integer>> getAdj_list() {
        return adj_list;
    }

    public void setAdj_list(ArrayList<LinkedList<Integer>> adj_list) {
        this.adj_list = adj_list;

    }

    public ArrayList<LinkedList<Integer>> addVertex() {
        adj_list.add(new LinkedList<Integer>());
        count++;
        return adj_list;

    }

    public ArrayList<LinkedList<Integer>> addAdjListOfVertex(int vertex, int vertexForAdjacencyLit) {

        if (vertex > count || vertexForAdjacencyLit > count) {
            System.out.println("Такой вершины нет");
            return null;
        } else {
            adj_list.get(vertex).add(vertexForAdjacencyLit);
        }
        return adj_list;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GraphAdj graphAdj = (GraphAdj) o;
        return Objects.equals(adj_list, graphAdj.adj_list);
    }

    @Override
    public int hashCode() {
        return Objects.hash(adj_list);
    }

    @Override
    public String toString() {
        return "GraphTest{" +
                "adj_list=" + adj_list +
                '}';
    }
}


