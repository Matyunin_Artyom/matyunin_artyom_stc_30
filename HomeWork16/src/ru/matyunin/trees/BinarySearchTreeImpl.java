package ru.matyunin.trees;

import java.util.*;

/**
 * 02.12.2020
 * 32. Trees
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class BinarySearchTreeImpl<T extends Comparable<T>> implements  BinarySearchTree<T> {

    static class Node<E> {
        E value;
        Node<E> left;
        Node<E> right;

        public Node(E value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value.toString();
        }
    }

    private Node<T> root;

    @Override
    public void insert(T value) {
        this.root = insert(this.root, value);
    }

    private Node<T> insert(Node<T> root, T value) {
        if (root == null) {
            root = new Node<>(value);
        }
        // значение меньше корня
        else if (value.compareTo(root.value) < 0) {
            // добавляем в левое поддерево
            root.left = insert(root.left, value);
        } else {
            root.right = insert(root.right, value);
        }
        return root;
    }

    @Override
    public void printDfs() {
        dfs(root);
    }

    @Override
    public void printDfsByStack() {
        Deque<Node<T>> stack = new LinkedList<>();
        stack.push(root);

        Node<T> current;

        while (!stack.isEmpty()) {
            current = stack.pop();
            System.out.println(current.value + " ");
            if (current.left != null) {
                stack.push(current.left);
            }
            if (current.right != null) {
                stack.push(current.right);
            }
        }
    }

    @Override
    public void printBfs() {
        Deque<Node<T>> queue = new LinkedList<>();
        queue.add(root);

        Node<T> current;

        while (!queue.isEmpty()) {


            System.out.print(Arrays.toString(queue.toArray()) + " ");
            current = queue.poll();
            if (current.left != null) {
                queue.add(current.left);
            }
            if (current.right != null) {
                queue.add(current.right);
            }
            System.out.println();

        }
    }

    private void dfs(Node<T> root) {
        System.out.println("in root = " + root);
        if (root != null) {
            dfs(root.left);
            System.out.println(root.value + " ");
            dfs(root.right);
        }
        System.out.println("from root = " + root);
    }

    @Override
    public boolean contains(T value) {
        Node<T> current = root;
        while(current.value!=value){
            if((value.compareTo(current.value)>0)){
                current = current.right;
            }else{
                current = current.left;
            }
            if(current==null){
                return false;
            }
        }
        return true;


      //  return false;
    }
    @Override
    public void remove(T value) {
        this.root = remove(this.root, value);
    }

    public Node<T> remove(Node<T> root, T value) {
        if (root == null) {
            return root;
        } else if (value.compareTo(root.value) < 0) {
            root.left = remove(root.left, value);
        } else if (value.compareTo(root.value) > 0) {
            root.right = remove(root.right, value);
        } else if (root.left != null && root.right != null){


        root.value = findMin(root.right).value;
        root.right = remove(root.right, root.value);
    } else if (root.left != null) {
        root = root.left;
    } else if (root.right != null) {
        root = root.right;
    } else {
        root = null;
    }
        return root;


    }

    protected Node<T> findMin( Node<T> root) {

        if( root != null ) {

            while (root.left != null) {

                root = root.left;
            }
        }


        return root;

    }

    public void printBfsByLevel() {

        Queue<Node<T>> firstQueue = new LinkedList<>();
        firstQueue.add(root);

        Queue<Queue<Node<T>>> mainQueue = new LinkedList<>();
        mainQueue.add(firstQueue);

        while (!mainQueue.isEmpty()) {
            Queue<Node<T>> levelQueue = mainQueue.poll();
            Queue<Node<T>> nextLevelQueue = new LinkedList<>();
            for (Node root : levelQueue) {
                System.out.print(root.value + " ");
                if (root.left != null)   {
                    nextLevelQueue.add(root.left);
                }
                if (root.right != null)  {
                    nextLevelQueue.add(root.right);
                }
            }
            if (!nextLevelQueue.isEmpty()) {
                mainQueue.add(nextLevelQueue);
            }
            System.out.println();
        }
    }



}
