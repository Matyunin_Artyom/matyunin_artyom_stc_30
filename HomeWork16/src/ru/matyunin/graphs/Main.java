package ru.matyunin.graphs;

public class Main {

    public static void main(String[] args) {
	    Vertex a0 = new VertexIntegerImpl(0);
	    Vertex a1 = new VertexIntegerImpl(1);
	    Vertex a2 = new VertexIntegerImpl(2);
	    Vertex a3 = new VertexIntegerImpl(3);
	    Vertex a4 = new VertexIntegerImpl(4);

        Edge edge0 = new EdgeTwoVerticesImpl(a0,a1, 3);
        Edge edge1 = new EdgeTwoVerticesImpl(a0,a4, 8);
        Edge edge2 = new EdgeTwoVerticesImpl(a0,a3, 7);
        Edge edge3 = new EdgeTwoVerticesImpl(a4,a3, 3);
        Edge edge4 = new EdgeTwoVerticesImpl(a1,a3, 4);
        Edge edge5 = new EdgeTwoVerticesImpl(a1,a2, 1);
        Edge edge6 = new EdgeTwoVerticesImpl(a3,a2, 2);

        OrientedGraph orientedGraph = new GraphAdjacencyMatrixImpl();
        orientedGraph.addEdge(edge0);
        orientedGraph.addEdge(edge1);
        orientedGraph.addEdge(edge2);
        orientedGraph.addEdge(edge3);
        orientedGraph.addEdge(edge4);
        orientedGraph.addEdge(edge5);
        orientedGraph.addEdge(edge6);

        orientedGraph.print();
    }
}
