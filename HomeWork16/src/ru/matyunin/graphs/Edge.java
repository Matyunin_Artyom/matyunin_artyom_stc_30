package ru.matyunin.graphs;

/**
 * 02.12.2020
 * 33. Graphs
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface Edge {
    Vertex getFirst();
    Vertex getSecond();
    int weight();
}
