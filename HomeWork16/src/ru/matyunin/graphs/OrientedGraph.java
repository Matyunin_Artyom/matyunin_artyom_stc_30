package ru.matyunin.graphs;

/**
 * 02.12.2020
 * 33. Graphs
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface OrientedGraph {
    void addVertex(Vertex vertex);
    void addEdge(Edge edge);
    boolean isAdjacent(Vertex a, Vertex b);
    void print();
}
