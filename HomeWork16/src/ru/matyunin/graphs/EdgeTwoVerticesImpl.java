package ru.matyunin.graphs;

/**
 * 02.12.2020
 * 33. Graphs
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class EdgeTwoVerticesImpl implements Edge {

    private Vertex first;
    private Vertex second;
    private int weight;

    public EdgeTwoVerticesImpl(Vertex first, Vertex second, int weight) {
        this.first = first;
        this.second = second;
        this.weight = weight;
    }

    @Override
    public Vertex getFirst() {
        return first;
    }

    @Override
    public Vertex getSecond() {
        return second;
    }

    @Override
    public int weight() {
        return weight;
    }
}
