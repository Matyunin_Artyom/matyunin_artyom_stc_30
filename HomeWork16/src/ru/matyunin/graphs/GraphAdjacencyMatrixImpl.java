package ru.matyunin.graphs;

/**
 * 02.12.2020
 * 33. Graphs
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class GraphAdjacencyMatrixImpl implements OrientedGraph {

    private static final int MAX_VERTICES_COUNT = 5;
    private int count = 0;

    private int matrix[][] = new int[MAX_VERTICES_COUNT][MAX_VERTICES_COUNT];

    @Override
    public void addVertex(Vertex vertex) {
        // TODO: сделать проверку
        count++;
    }

    @Override
    public void addEdge(Edge edge) {
        // проверить, ест ли нужная вершина в графе уже
        matrix[edge.getFirst().getNumber()][edge.getSecond().getNumber()] = edge.weight();
    }

    @Override
    public boolean isAdjacent(Vertex a, Vertex b) {
        return false;
    }

    @Override
    public void print() {
        System.out.print("   ");
        for (int i = 0; i < matrix.length; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
        for (int i = 0; i < matrix.length; i++) {
            System.out.print(i + "  ");
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}
