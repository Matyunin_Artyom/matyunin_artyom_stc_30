package ru.matyunin.graphs;

/**
 * 02.12.2020
 * 33. Graphs
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class VertexIntegerImpl implements Vertex {

    private int number;

    public VertexIntegerImpl(int number) {
        this.number = number;
    }

    @Override
    public int getNumber() {
        return number;
    }
}
