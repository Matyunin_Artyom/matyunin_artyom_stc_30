package ru.matyunin.single;

public interface NumberProcess {
    public int process(int number);
}
