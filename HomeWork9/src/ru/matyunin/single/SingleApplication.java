package ru.matyunin.single;

public class SingleApplication {

    public static void main(String[] args) {
        int number = 5409;
        //замена нечетных чисел четными
        NumberProcess numberProcessReverse = reverce-> {
            int checkNumber = reverce % 10;
            reverce = reverce / 10;
            while (reverce >= 0) {
                if (checkNumber % 2 != 0) {
                    checkNumber--;
                }
                checkNumber = checkNumber * 10 + reverce % 10;
                if (reverce==0){
                    break;}
                reverce = reverce / 10;
            }
            reverce = checkNumber % 10;
            checkNumber = checkNumber / 10;
            while (checkNumber != 0) {
                reverce = reverce * 10 + checkNumber % 10;
                checkNumber = checkNumber / 10;
            }
            return reverce;
        };
        //разворот числа
        NumberProcess numberProcessReplaceNull = numberReplaceNull -> {
            int checkNumber = numberReplaceNull % 10;
            numberReplaceNull = numberReplaceNull / 10;

            while (numberReplaceNull != 0) {
                checkNumber = checkNumber * 10 + numberReplaceNull % 10;
                numberReplaceNull = numberReplaceNull / 10;
            }
            return numberReplaceNull = checkNumber;
        };
        //удаление нулей
        NumberProcess numberProcessOddToEven = oddToEven -> {
            int checkNumber = oddToEven % 10;
            oddToEven = oddToEven / 10;
            int count = 1;
            while (oddToEven != 0) {
                 while (oddToEven % 10 == 0) {
                    oddToEven = oddToEven / 10;
                }
                checkNumber = checkNumber * 10 + oddToEven % 10;
                oddToEven = oddToEven / 10;
            }
            oddToEven = checkNumber % 10;
            checkNumber = checkNumber / 10;
            while (checkNumber != 0) {
                while (checkNumber % 10 == 0) {
                    checkNumber= checkNumber / 10;
                }
                oddToEven = oddToEven * 10 + checkNumber % 10;
                checkNumber = checkNumber / 10;
            }
            return oddToEven;
        };
        System.out.println("Changed number: ");
        System.out.println(numberProcessReverse.process(number));
        System.out.println(numberProcessReplaceNull.process(number));
        System.out.println(numberProcessOddToEven.process(number));

        String string = "r2D2.";
        //разворот строки
        StringProcess stringProcessReverse = processReverse -> {
            String result = "";
            for (int i = 0; i < processReverse.length(); i++) {
                result = processReverse.charAt(i) + result;
            }
            return processReverse = result;
        };
        //сделать маленькие буквы болшими
        StringProcess stringProcessUppercase = processUppercase -> processUppercase.toUpperCase();
        //удалить цифры из числа
        StringProcess stringProcessIsLetter = processIsLetter -> {
            String result = "";
            for (int i = 0; i < processIsLetter.length(); i++) {
                char checkLetter = processIsLetter.charAt(i);
                if (Character.isLetter(checkLetter)) {
                    result = result + checkLetter;
                }
            }
            return processIsLetter = result;
        };
        System.out.println("Changed string:");
        System.out.println(stringProcessReverse.process(string));
        System.out.println(stringProcessUppercase.process(string));
        System.out.println(stringProcessIsLetter.process(string));
    }
}


