package ru.matyunin.arrays;

public class ArraysApplication {

    public static void main(String[] args) {
        int[] numbersArray = {12, 3201, 45, 123, 650, 203, 4, 501, 65004, 105, 2078, 308, 105, 203};
        NumbersAndStringProcessor numbersProcessor = new NumbersAndStringProcessor(numbersArray);
        numbersProcessor.modifiedNumbersOfArray(numberReverse -> {
            int checkNumber = numberReverse % 10;
            numberReverse = numberReverse / 10;
            while (numberReverse != 0) {
                checkNumber = checkNumber * 10 + numberReverse % 10;
                numberReverse = numberReverse / 10;
            }
            return checkNumber;
        }).modifiedNumbersOfArray(numberReplaceNull -> {
            int checkNumber = numberReplaceNull % 10;
            numberReplaceNull = numberReplaceNull / 10;
            while (numberReplaceNull != 0) {
                while (numberReplaceNull % 10 == 0) {
                    numberReplaceNull = numberReplaceNull / 10;
                }
                checkNumber = checkNumber * 10 + numberReplaceNull % 10;
                numberReplaceNull = numberReplaceNull / 10;
            }
            numberReplaceNull = checkNumber % 10;
            checkNumber = checkNumber / 10;
            while (checkNumber != 0) {
                while (checkNumber % 10 == 0) {
                    checkNumber = checkNumber / 10;
                }
                numberReplaceNull = numberReplaceNull * 10 + checkNumber % 10;
                checkNumber = checkNumber / 10;
            }
            return numberReplaceNull;
        }).modifiedNumbersOfArray(numberOddToEven -> {
            int checkNumber = numberOddToEven % 10;
            numberOddToEven = numberOddToEven / 10;
            while (numberOddToEven >= 0) {
                if (checkNumber % 2 != 0) {
                    checkNumber--;
                }
                checkNumber = checkNumber * 10 + numberOddToEven % 10;
                if (numberOddToEven == 0) {
                    break;
                }
                numberOddToEven = numberOddToEven / 10;
            }
            numberOddToEven = checkNumber % 10;
            checkNumber = checkNumber / 10;
            while (checkNumber != 0) {
                numberOddToEven = numberOddToEven * 10 + checkNumber % 10;
                checkNumber = checkNumber / 10;
            }
            return numberOddToEven;
        })
                .printNumberArray(process -> System.out.print(process + " "));
        System.out.println();

        String[] stringsArray = {"12xxv", "h0d01", "r2D2", "321l", "Hello", "Bay", "abcde", "Firstname", "Lastname"};
        NumbersAndStringProcessor numbersStringProcessor = new NumbersAndStringProcessor(stringsArray);
        numbersStringProcessor.modifiedStringsOfArray(processReverse -> {
            String result = "";
            for (int i = 0; i < processReverse.length(); i++) {
                result = processReverse.charAt(i) + result;
            }
            return result;
        }).modifiedStringsOfArray(processUpperCase -> processUpperCase.toUpperCase())
                .modifiedStringsOfArray(processReplaceDigit -> {
                    String result = "";
                    for (int i = 0; i < processReplaceDigit.length(); i++) {
                        char checkLetter = processReplaceDigit.charAt(i);
                        if (Character.isLetter(checkLetter)) {
                            result = result + checkLetter;
                        }
                    }
                    return result;
                })
                .printStringArray(process -> System.out.print(process + " "));


    }

}
