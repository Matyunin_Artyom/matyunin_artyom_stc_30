package ru.matyunin.arrays;

public interface NumberProcess {
    public int process(int number);
}
