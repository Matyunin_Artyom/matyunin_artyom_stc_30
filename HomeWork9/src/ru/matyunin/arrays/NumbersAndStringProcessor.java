package ru.matyunin.arrays;

public class NumbersAndStringProcessor {
    private int[] numbers;
    private String[] strings;

    public NumbersAndStringProcessor(int[] numbers) {

        this.numbers = numbers;
    }
    public NumbersAndStringProcessor(String[] strings) {

        this.strings = strings;
    }
    public NumbersAndStringProcessor modifiedNumbersOfArray(NumberProcess numberProcess) {
        for (int i = 0; i < numbers.length; i++) {
            this.numbers[i] = numberProcess.process(this.numbers[i]);
        }
        return this;
    }
    public NumbersAndStringProcessor modifiedStringsOfArray(StringProcess stringProcess) {
        for (int i = 0; i < strings.length; i++) {
            this.strings[i] = stringProcess.process(this.strings[i]);
        }
        return this;
    }
    void printNumberArray(PrintNumberArray printArray) {
        System.out.println("Changed elements of an array of numbers:");
        for (int i = 0; i<this.numbers.length; i++) {
            printArray.printNumberArray(numbers[i]);
        }
    }
    void printStringArray(PrintStringArray printArray) {
        System.out.println("Changed elements of a string array:");
        for (int i = 0; i<this.strings.length; i++){
            printArray.printStringArray(strings[i]);
        }
    }

}
