package ru.matyunin;

public class MainBuilderPattern {

    public static void main(String[] args) {
	// write your code here
        User user = new User.Builder()
                .firstName("Ivan")
                .lastName("Ivanov")
                .age(45)
                .isWorker(true)
                .build();
        user.printUser();


    }
}
