package ru.matyunin;

public class User {

    private String firstName = "";
    private String lastName = "";
    private int age = 0;
    private boolean isWorker = false;

    public static class Builder {
        private User newUser;

        public Builder() {
            newUser = new User();
        }

        public Builder firstName(String firstName) {
            newUser.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            newUser.lastName = lastName;
            return this;
        }

        public Builder age(int age) {
            newUser.age = age;
            return this;
        }

        public Builder isWorker(boolean isWorker) {
            newUser.isWorker = isWorker;
            return this;
        }

        public User build() {
            return newUser;
        }

    }

    public void printUser() {
        System.out.println("First Name: " + "'" + firstName + "'");
        System.out.println("Last Name: " + "'" + lastName + "'");
        System.out.println("Age: " + "'" + age + "'");
        System.out.println("Is worker " + "'" + isWorker + "'");
    }

}
